# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from openpyxl.utils import column_index_from_string
from openpyxl.worksheet.dimensions import ColumnDimension
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.AssignCSV import AssignCSV
from formatClass.ProjectCSV import ProjectCSV
from formatClass.OrganizationEXCEL import OrganizationEXCEL as orgxls
from formatClass.OrganizationEXCEL import JobTitleLank
from formatClass.BaseInfo import BaseInfo

class ProfitGroup :
    
    def __init__( self, name, leader, key, code=None, projects=[], detail=False ) :
        self.name = name
        self.leader = leader
        self.key = key
        self.total_amount = 0
        self.total_profit = 0
        self.total_damount = 0
        self.total_conti = 0
        self.total_jamount = 0
        self.total_jdamount = 0
        self.members = []
        self.children = []
        self.cost_code = code
        self.detail = detail
        self.profit = None
        self.projects = projects
        
    def add( self, profit ) :
        self.children.append(profit)

    def addMember( self, member ) :
        if not member.kenmu :
            name = JBBot.removeSpace(member.name)
            self.members.append(name)

    def memberCount( self ) :
        count = len(self.members)
        for child in self.children:
            count += child.memberCount()
        return count

    def hasCostCode( self, code ) :
        if self.cost_code == code : return True
        for child in self.children:
            if child.cost_code == code : return True
        return False

    def appendAmount(self, team) :
        self.total_amount += team.amount
        self.total_profit += team.profit
        self.total_damount += team.d_amount
        self.total_conti += team.conti
        self.total_jamount += team.j_amount
        self.total_jdamount += team.jd_amount
        profit = ProfitGroup(team.leader,team.leader,team.cost_code+'_'+team.id, None, team.projects, True)
        profit.total_amount += team.amount
        profit.total_profit += team.profit
        profit.total_damount += team.d_amount
        profit.total_conti += team.conti
        profit.total_jamount += team.j_amount
        profit.total_jdamount += team.jd_amount

        self.add(profit)
    
    def addAmount( self, team, top=False ) :
        if not top and self.cost_code and self.cost_code != team.cost_code : return False
        if self.leader == team.leader or team.leader in self.members :
            self.appendAmount(team)
            return True
        else :
            for child in self.children :
                if child.addAmount(team) :
                    self.total_amount += team.amount
                    self.total_profit += team.profit
                    self.total_damount += team.d_amount
                    self.total_conti += team.conti
                    self.total_jamount += team.j_amount
                    self.total_jdamount += team.jd_amount
                    return True
                
        if self.cost_code == team.cost_code :
            self.appendAmount(team)
            return True
        
        return False
    
    def printResult( self, output, groupmode, thisFS, diffResults, withPlan=False ) :
        if self.detail :
            output.skipCell(2)
            output.stringCell(self.name)
        else :
            if groupmode:
                output.stringCell("")
                output.stringCell(self.name)
                output.stringCell(self.leader)
            else:
                output.stringCell(self.leader)
                output.skipCell(2)
        output.moneyCell(self.total_amount)
        output.moneyCell(self.total_profit)
        output.ratioCalcCell(self.total_profit, self.total_amount)
        if self.detail :
            output.skipCell(3)
        else :
            output.stringCell(str(self.memberCount()))
            output.moneyCalcCell(self.total_amount,self.memberCount())
            output.moneyCalcCell(self.total_profit,self.memberCount())
        output.stringCell(self.key)
        if thisFS :
            if self.detail :
                output.skipCell(1)
                if self.total_damount > 0.1 :
                    output.stringCell("***",None,Align.RIGHT)
                else :
                    output.stringCell("")
                output.skipCell(1)
                if self.total_conti > 0.1 :
                    output.stringCell("***",None,Align.RIGHT)
                else :
                    output.stringCell("")
                output.skipCell(1)
            else :
                output.stringCell("")
                output.moneyCell(self.total_damount)
                output.ratioCalcCell(self.total_damount, self.total_amount)
                
                output.moneyCell(self.total_conti)
                output.ratioCalcCell(self.total_conti+self.total_profit, self.total_amount)
            
            if len(diffResults) > 0 :
                ProfitGroup.printResultDiff(self, output, diffResults[0]['dict'], 0 )
            else :
                output.stringCell("")
                output.moneyCell(self.total_jamount)
                output.ratioCalcCell(self.total_jdamount, self.total_jamount)
                
            if len(diffResults) > 1 :
                ProfitGroup.printResultDiff(self, output, diffResults[1]['dict'], 0 )
        output.nextRow()
        if self.detail :
            projectdicts = []
            pmkey = 'PD_'+self.key
            for diffResult in diffResults :
                prevdict = diffResult['dict']
                if pmkey in prevdict :
                    projectdicts.append(prevdict[pmkey])
                else :
                    projectdicts.append(None)
                    
            for project in self.projects :
                output.skipCell(3)
                output.stringCell(project.name,Fill.LIGHTGRAY,Align.LEFT)
                output.skipCell(3, Fill.LIGHTGRAY)
                output.moneyCell(project.total_amount,Fill.LIGHTGRAY)
                output.moneyCell(project.total_profit,Fill.LIGHTGRAY)
                output.stringCell(self.key+":"+project.key,Fill.LIGHTGRAY)
                output.setRowDimensions(1)
                if thisFS :
                    output.ratioCalcCell(project.total_profit, project.total_amount, Fill.LIGHTGRAY, Align.RIGHT)
                    if project.total_damount > 0.1 :
                        output.moneyCell(project.total_damount,Fill.LIGHTGRAY)
                    else:
                        output.skipCell(1,Fill.LIGHTGRAY)
                    output.stringCell(project.grade,Fill.LIGHTGRAY,Align.LEFT)
                    if project.total_conti > 0.1 :
                        output.moneyCell(project.total_conti, Fill.LIGHTGRAY)
                        output.ratioCalcCell(project.total_conti+project.total_profit, project.total_amount,Fill.LIGHTGRAY)
                    else:
                        output.skipCell(2,Fill.LIGHTGRAY)

                    if len(diffResults) > 0 :
                        offset = 0
                        for projectdict in projectdicts :
                            ProfitGroup.printResultDiff(project, output, projectdict, offset, Fill.LIGHTGRAY )
                            offset = 0
                    else :
                        output.skipCell(1,Fill.LIGHTGRAY)
                        output.moneyCell(project.total_jamount,Fill.LIGHTGRAY)
                        
                output.nextRow()
            if thisFS :
                offset = 0
                for projectdict in projectdicts :
                    if not projectdict : continue 
                    for projectdiff in projectdict.values() :
                        output.skipCell(3)
                        output.stringCell( projectdiff.title, Fill.LIGHTGRAY, Align.LEFT )
                        output.skipCell(6, Fill.LIGHTGRAY )
                        output.stringCell("削除", Fill.LIGHTGRAY )
                        output.skipCell(5, Fill.LIGHTGRAY )
                        output.setRowDimensions(1)
                        if offset > 0 : output.skipCell( offset )
                        output.moneyCell( -projectdiff.amount, Fill.LIGHTGRAY )
                        output.moneyCell( -projectdiff.profit, Fill.LIGHTGRAY )
                        if projectdiff.d_amount :
                            output.moneyCell( -projectdiff.d_amount, Fill.LIGHTGRAY )
                        else :
                            output.stringCell('', Fill.LIGHTGRAY )
                        output.nextRow()
                    offset += 4
        if withPlan and self.key in BaseInfo.plans:
            plan = BaseInfo.plans[self.key]
            output.skipCell(1)
            output.stringCell(BaseInfo.plantitle+"差",Fill.LIGHTGRAY,Align.RIGHT)
            output.skipCell(1, Fill.LIGHTGRAY)
            output.moneyCell( self.total_amount - plan.amount ,Fill.LIGHTGRAY )
            output.moneyCell( self.total_profit - plan.profit ,Fill.LIGHTGRAY )
            output.nextRow()
            
    @staticmethod
    def printResultDiff( target, output, diffDict, skip, fill=None):
        diffamount = 0
        diffprofit = 0
        diffdamount = None
        diffconti = None
        if diffDict :
            if target.key in diffDict:
                diffData = diffDict.pop(target.key)
                diffamount = diffData.amount
                diffprofit = diffData.profit
                if diffData.d_amount :
                    diffdamount = diffData.d_amount
                if diffData.conti :
                    diffconti = diffData.conti
        if skip :
            output.skipCell(skip,fill)
        output.stringCell("",fill)
        output.moneyCell( target.total_amount - diffamount,fill )
        output.moneyCell( target.total_profit - diffprofit,fill )
        if target.total_damount and target.total_damount != "***" and diffdamount and diffdamount != "***":
            output.moneyCell( target.total_damount - float(diffdamount),fill )
        else :
            output.stringCell('',fill)
        if target.total_conti and target.total_conti != "***" and diffconti and diffconti != "***":
            output.moneyCell( target.total_conti - float(diffconti),fill )
        else :
            output.stringCell('',fill)

    @staticmethod
    def makeTeamAmount(project_managers, stddate) :
        termstart = JBDate.getFSStartDate(stddate)
        termend = JBDate.getFSEndDate(stddate)
        print(termstart)
        print(termend)	
        TeamAmount = namedtuple('TeamAmount' , ['leader','id','cost_code','amount','profit','d_amount', 'conti','projects', 'j_amount','jd_amount'])
        DetailProject = namedtuple('DetailProject', [ 'name', 'key', 'grade','total_amount','total_profit','total_damount','total_conti','total_jamount'])
        AMOUNT = 0
        PROFIT = 1
        D_AMNT = 2
        CONTI  = 3
        J_AMNT = 4
        teams = []
        for manager in project_managers.values() :
            projects = []
            amount = 0
            profit = 0
            conti = 0
            d_amount = 0
            j_amount = 0
            jd_amount = 0
            contracts = manager.contracts
            contracts.sort(key=lambda x:x.project_id)
            projectid = None
            grade = None
            title = None
            pamounts = [0] * 5
            for contract in contracts :
                #BaseInfo の補正情報から、対象外を除外
                if contract.id in BaseInfo.deletecodes : continue

                if contract.forcast_month <= termend : 
                    amount += ProjectCSV.getAmount(contract)
                    profit += ProjectCSV.getProfit(contract)
                    conti += ProjectCSV.getConti(contract)
                    if contract.grade == 'D':
                        d_amount += contract.forcast_amount

                    _cont_j_amount = 0
                    if contract.forcast_order >= termstart and contract.forcast_order <= termend:
                        _cont_j_amount = ProjectCSV.getAmount(contract)
                        j_amount += _cont_j_amount
                        if contract.grade == 'D':
                            jd_amount += contract.forcast_amount
                    
                    # ojectIDがない場合は、J/F番号でエントリー
                    if not contract.project_id :
                        projects.append( DetailProject( contract.title, contract.id, contract.grade,
                                                        ProjectCSV.getAmount(contract),
                                                        ProjectCSV.getProfit(contract),
                                                        contract.forcast_amount if contract.grade == 'D' else 0,
                                                        ProjectCSV.getConti(contract), _cont_j_amount))
                        continue
                    # ProjectID で集約
                    else :
                        if projectid is None :
                            projectid = contract.project_id
                            title = contract.project_name
                            grade = contract.grade
                        elif projectid != contract.project_id :
                            projects.append( DetailProject( title, projectid, grade,
                                                            pamounts[AMOUNT], pamounts[PROFIT],
                                                            pamounts[D_AMNT], pamounts[CONTI], pamounts[J_AMNT]))
                            projectid = contract.project_id
                            title = contract.project_name
                            if contract.grade not in grade :
                                grade = grade+','+contract.grade
                            pamounts = [0] * 5
                        
                        pamounts[AMOUNT] += ProjectCSV.getAmount(contract)
                        pamounts[PROFIT] += ProjectCSV.getProfit(contract)
                        pamounts[CONTI] += ProjectCSV.getConti(contract)
                        pamounts[J_AMNT] += _cont_j_amount
                        if contract.grade == 'D':
                            pamounts[D_AMNT] += contract.forcast_amount
                else : # 受注のみ
                    if contract.forcast_order >= termstart and contract.forcast_order <= termend:
                        j_amount += ProjectCSV.getAmount(contract)
                        if contract.grade == 'D':
                            jd_amount += contract.forcast_amount
                        projects.append( DetailProject( contract.title, contract.id, contract.grade,
                                                        0,0,0,0,ProjectCSV.getAmount(contract)))
            if pamounts[AMOUNT] > 0 :
                projects.append( DetailProject( title, projectid, grade,
                                                pamounts[AMOUNT], pamounts[PROFIT],
                                                pamounts[D_AMNT], pamounts[CONTI], pamounts[J_AMNT] ))
            if len(projects) == 0 : continue
            name = JBBot.removeSpace(manager.name)
            teams.append( TeamAmount( name, manager.id, manager.cost_code, amount, profit, d_amount, conti, projects, j_amount, jd_amount))

        return teams

    # 結果を集約する事業部組織構造を作成
    @staticmethod
    def makeResultDivisionStructure( divisions ) :
        results = []
        divprofit = None
        for division in orgxls.getDivisions() :
            orgmenbers = orgxls.getDivisionMember( division, False )
            name = JBBot.removeSpace(orgmenbers[0].name)
            costcode = BaseInfo.getCostCode(division,name)
            if not costcode : continue
            
            divprofit = ProfitGroup(division, name, costcode, costcode) # 事業部
            results.append(divprofit)

            section = None
            section_costcode = costcode
            secprofit = None
            grpprofit = None
            groupno = 0
            for member in orgmenbers :
                name = JBBot.removeSpace(member.name)
                if section != member.section :
                    groupno = 0
                    grpprofit = None
                    section = member.section
                    section_costcode = BaseInfo.getCostCode(section, name)
                    if not section_costcode :
                        print('There is no costcode for '+ section+"_"+name+", so skip it!");
                        continue
                    secprofit = ProfitGroup(section, name, section_costcode , section_costcode) # 部
                    divprofit.add(secprofit)
                    continue
                
                if member.lank is JobTitleLank.GL :
                    groupno += 1
                    key = section_costcode
                    key = key + '-GRP'
                    key += str(groupno)
                    grpprofit = ProfitGroup(member.jobtitle, name, key, None) # グループ
                    if secprofit : secprofit.add(grpprofit)


                if grpprofit :
                    grpprofit.addMember(member) # 通常のグループ所属
                elif secprofit :
                    secprofit.addMember(member) # 部付き
                #else :
                #    divprofit.addMember(member) # 事業部付き
                    
        return results

def readDiffTarget(basedir, diffTarget):
    prevdata = {}
    DiffAmount = namedtuple('DiffAmounts', ['key','title','amount','profit', 'd_amount','conti'])
    result_dir = config.get('result', 'output_dir')
    type_dir = config.get('result', JBBot.appname())
    if type_dir :
        result_dir = result_dir + '/' + type_dir
    workbook = openpyxl.load_workbook(filename=result_dir+"/"+diffTarget)
    diffdate = None
    for worksheet in workbook.worksheets :        
        print('reading previous data for '+worksheet.title)
        if not diffdate : diffdate = worksheet.cell(row=1, column=1).value
        for row in worksheet.rows :
            key = row[column_index_from_string('J')-1].value
            title = row[column_index_from_string('D')-1].value
            if key :
                damount = row[column_index_from_string('L')-1].value
                conti = row[column_index_from_string('N')-1].value
                if row[0].value or row[2].value :
                    amount = row[column_index_from_string('D')-1].value
                    profit = row[column_index_from_string('E')-1].value
                    prevdata[key] = DiffAmount(key, title, amount, profit, damount, conti )
                else :
                    amount = row[column_index_from_string('H')-1].value
                    profit = row[column_index_from_string('I')-1].value
                    keys = key.split(':')
                    projectkey = 'PD_'+keys[0]
                    if projectkey in prevdata :
                        details = prevdata[projectkey]
                    else :
                        details = {}
                        prevdata[projectkey] = details
                    details[keys[1]] = DiffAmount(key, title, amount, profit, damount, conti ) 

    return { 'dict':prevdata, 'date':diffdate }

def organizationProductivity( base_dir, diffTarget, excel ) :
    orgxls.setup(config, base_dir, True)
    BaseInfo.setup(config, base_dir)

    diffResults = []

    diffDate = None
    if diffTarget :
        dresult = readDiffTarget(base_dir, diffTarget)
        diffResults.append(dresult);
        diffDate = dresult['date']
    
    diffBase = None
    if config.has_option('data', 'profit_base') :
        diffBase = config.get('data', 'profit_base')
        prefix = config.get('prefix', JBBot.appname())
        if prefix :
            diffBase = prefix + '_' + diffBase + ".xlsx"
        bresult = readDiffTarget(base_dir, diffBase)
        diffResults.append(bresult);

    ProjectCSV.setup(config, base_dir)
    stdDate = ProjectCSV.getStdDate()
    projects = ProjectCSV.selectByTermIncludeOrder(stdDate)
    project_managers = ProjectCSV.makeMembersDict(projects, BaseInfo.transfers)

    # 最小単位のプロジェクトマネージャチームで収益を集約
    teams = ProfitGroup.makeTeamAmount(project_managers, stdDate)

    # 結果を集約する事業部組織構造を作成
    divisions = ProfitGroup.makeResultDivisionStructure( orgxls.getDivisions() )

    # プロジェクトマネージャーチーム別の集約結果を、事業部の組織構造にマップする
    for team in teams :
        linked = False
        for division in divisions :
            if division.hasCostCode(team.cost_code) :
                if not division.addAmount(team, True) :
                    division.appendAmount(team)
                linked = True
        #if not linked :
        #    print("No Link: "+team.leader+"("+team.cost_code+")");

    # 結果を出力
    #thisFS = fsstart == JBDate.getFYString()
    thisFS = True
    # title部
    titleCells = [
        ['A', "{0:%Y/%m/%d}".format(stdDate) ],
        ['D', "受注A-D案件"],
    ]
    if thisFS :
        titleCells.extend([
            ['K', "内D案件"]
        ])
        titleCells.extend([
            ['N', "コンチ"]
        ])
        if len(diffResults) > 0 :
            titleCells.extend([
                ['Q', diffResults[0]['date']]
            ])
        if len(diffResults) > 1 :
            titleCells.extend([
                ['T', diffResults[1]['date']]
            ])

    subTitleCells = [
        ['C', "明細" ],
        ['D', "プロジェクト名", Fill.LIGHTGRAY],
        ['E', "", Fill.LIGHTGRAY],
        ['F', "", Fill.LIGHTGRAY],
        ['G', "", Fill.LIGHTGRAY],
        ['H', "売上", Fill.LIGHTGRAY, Align.RIGHT],
        ['I', "粗利", Fill.LIGHTGRAY, Align.RIGHT],
        ['K', "粗利率", Fill.LIGHTGRAY, Align.RIGHT]]
    if thisFS :
        subTitleCells.extend([
            ['L', "〃", Fill.LIGHTGRAY, Align.RIGHT],
            ['M', "確度", Fill.LIGHTGRAY, Align.RIGHT],
            ['N', "〃", Fill.LIGHTGRAY, Align.RIGHT],
            ['O', "〃", Fill.LIGHTGRAY, Align.RIGHT]
        ])
            
    # 出力ヘッダー部
    headerCells = [
        ['A',5, ''],
        ['B',10,''],
        ['C',10,''],
        ['D',17,'売上', Align.RIGHT],
        ['E',17,'粗利', Align.RIGHT],
        ['F',6,'利益率', Align.RIGHT],
        ['G',5,'人員', Align.RIGHT],
        ['H',15,'生産性(売上)', Align.RIGHT],
        ['I',15,'生産性(利益)', Align.RIGHT],
        ['J',25,'']]
    if thisFS :
        headerCells.extend([
            ['K',6,'',Align.CENTER],
            ['L',15,'D売上',Align.RIGHT],
            ['M',7,'比率', Align.RIGHT]])
        headerCells.extend([
            ['N',15,'コンチ額',Align.RIGHT],
            ['O', 6,'MAX率', Align.RIGHT]])
        if len(diffResults) > 0 :
            headerCells.extend([
                ['P',2,''],
                ['Q',15,'売上差',Align.RIGHT],
                ['R',15,'利益差',Align.RIGHT],
                ['S',15,'D売上差', Align.RIGHT],
                ['T',15,'コンチ差', Align.RIGHT]])
        else :
            headerCells.extend([
                ['P',2,''],
                ['Q',15,'受注額',Align.RIGHT],
                ['R',7,'D比率',Align.RIGHT]])

        if len(diffResults) > 1 :
            headerCells.extend([
                ['U',2,''],
                ['V',15,'売上差',Align.RIGHT],
                ['W',15,'利益差',Align.RIGHT],
                ['X',15,'D売上差', Align.RIGHT],
                ['Y',15,'コンチ差', Align.RIGHT]])

    spanEnd = 'R' if thisFS else 'J'

    prefix = config.get('prefix', JBBot.appname());

    filename = prefix+'_'+'{0:%Y%m%d}'.format(stdDate);
    if diffDate :
        filename += '-'+diffDate.replace('/','')
    
    for division in divisions :
        output = JBOutput(filename, base_dir+'_'+division.name, config)
        output.excelOutput(excel)
        output.headerTitles(titleCells)
        output.headerRow(headerCells)
        output.headerTitles(subTitleCells)
        print('making for '+ division.name)

        #事業部
        output.titleRow(division.name,['A',spanEnd])
        division.printResult(output, False, thisFS, diffResults,True)

        for section in division.children:
            if section.detail :
                section.printResult(output, False, thisFS, diffResults)
                
        #各部（原価部門）
        for section in division.children:
            if not section.detail :
                output.titleRow(section.name,['A',spanEnd])
                section.printResult(output, False, thisFS, diffResults,True) 
                
            for group in section.children:
                if group.detail :
                    group.printResult(output, False, thisFS, diffResults) 
            
            for group in section.children:
                if not group.detail :
                    group.printResult(output, True, thisFS, diffResults) 
                
                for team in group.children:
                    team.printResult(output, False, thisFS, diffResults) 
        
        output.hideCol('J');
        output.save()
    
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')

    prefix = config.get('prefix', JBBot.appname())
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        print("\t-e: Excel output")
        exit(0)

    baseDir = JBDate.getFYString()

    if JBBot.getOption('-b', True) :
        baseDir = JBBot.getOption('-b')

    diffTarget = None
    if JBBot.getOption('-d', True) == True :
        targets = JBBot.targetFiles(prefix, config)
        diffTarget = JBBot.selection(targets)

    organizationProductivity( baseDir,
                              diffTarget,
                              JBBot.getOption('-e',True))
