import datetime
from openpyxl.styles.alignment import Alignment

class ProjectGroup:
    def __init__(self, index, termlen) :
        self.rowindex = index
        self.total_amount = 0
        self.month_amounts = [0]*termlen
        self.month_members = [0]*termlen
        self.members = {}


    def putHeader( self, ws, col, name ) :
        print(name)
        index = self.rowindex
        index += 1
        ws[col+str(index)] = name
        index += 2 #month_amouns and month_amount_par_member
        return index

    def putMembers( self, ws, col, gterms ) :
        index = self.rowindex + 2 
        for member in self.members.values() :
           index += 1
           member.putAssign(ws, col, index, gterms, self.month_members)
        index += 1
        for i, ma in enumerate(self.month_members) :
           ws[chr(ord(col)+i+1)+str(index)] = ma
        return index

    def putHeaderContent( self, ws, col, termstart_col ) :
       	header_index = self.rowindex + 1 
        ws[col+str(header_index)] = self.total_amount
        ws[col+str(header_index)].number_format = '#,##0'

        for i, ma in enumerate(self.month_amounts) :
            ws[chr(ord(termstart_col)+i+1)+str(header_index)] = ma
            ws[chr(ord(termstart_col)+i+1)+str(header_index)].number_format = '#,##0'
            month_member = self.month_members[i]
            if month_member > 0 :
                permonth = ma / month_member
                ws[chr(ord(termstart_col)+i+1)+str(header_index + 1)] = permonth
                ws[chr(ord(termstart_col)+i+1)+str(header_index + 1)].number_format = '#,##0'
            elif ma > 0 :
                ws[chr(ord(termstart_col)+i+1)+str(header_index + 1)] = '***'  
                ws[chr(ord(termstart_col)+i+1)+str(header_index + 1)].alignment = Alignment(horizontal = 'right')

            
    def addAmount( self, amount, start, end, start_month ) :
        self.total_amount += float(amount)
        months = (end.year - start.year ) * 12 + end.month - start.month + 1
        month_amount = float(amount)/months
        tyear = start_month.year
        tmonth = start_month.month
        for i, ma in enumerate(self.month_amounts) :
            thismonth = datetime.datetime(tyear, tmonth, 1)
            if start <= thismonth and end >= thismonth : self.month_amounts[i] += month_amount
            tmonth = tmonth + 1
            if tmonth > 12 :
                tmonth = 1
                tyear = tyear + 1

    def addMember( self, pmember ) :
        member = None
        if pmember.id in self.members :
            member = self.members[pmember.id]
            member.margeTermAsign(pmember)
        else :
            member = pmember
            self.members[pmember.id] = member
