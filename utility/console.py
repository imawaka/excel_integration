# -*- coding: utf-8 -*-
import locale
import unicodedata


class JBConsoleError(Exception):
    pass

#
# 日本語用のljust
#
def left(string,length):
    if string :
        for c in string:
            if unicodedata.east_asian_width(c) in ('F', 'W', 'A'):
                length -= 2
            else:
                length -= 1
    else : string = ''
    return string + ' '*length

def right(string,length):
    for c in string:
        if unicodedata.east_asian_width(c) in ('F', 'W', 'A'):
            length -= 2
        else:
            length -= 1
    return ' '*length + string

def center(string,length):
    for c in string:
        if unicodedata.east_asian_width(c) in ('F', 'W', 'A'):
            length -= 2
        else:
            length -= 1
    if length % 2 == 0:
        left = length / 2
        right = left
    else :
        left = (length - 1) / 2
        right = left + 1
    return ' '*int(left) + string + ' '*int(right)


def longTitle(title, width):
    if not title : title = ""
    length = width
    adjust = False
    shaped = ""
    postfix = ""
    for c in title:
        if unicodedata.east_asian_width(c) in ('F', 'W', 'A'):
            length -= 2
        else:
            length -= 1
            
        if length > 3:
            shaped += c
        else:
            if len(postfix) == 0 and length == 3: adjust = True
            postfix += c
            
    if postfix :
        if length >= 0 :
            shaped = shaped + postfix
        else :
            shaped += "... "
            if adjust: shaped += " "

    return shaped + ' '*length
    
def money(amount):
    return right(str(locale.currency(amount,grouping=True,symbol=False)),12)

def ratio(amount):
    return right(str(int(amount*100))+'%',10)

def date(date):
    return date.strftime('%Y/%m/%d')
