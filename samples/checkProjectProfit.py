# -*- coding: utf-8 -*-
from collections import namedtuple
from utility import JBBot, JBDate, console
from itertools import groupby
from formatClass.ProjectCSV import ProjectCSV


def checkProject( config, argdir = None ) :
    baseDir = JBDate.getFYString()
    ProjectCSV.setup(config, baseDir)
    
    project_dir = JBBot.selectDir( config.get('data', 'base_dir') + "/" + baseDir + "/" +
                                   config.get('data', 'project_dir'), argdir)

    stdDate = JBBot.getDateFromPath(project_dir)
    lastMonth = JBDate.getLastMonth(stdDate);
    
    ProjectCSV.readDir(project_dir)

    projects = ProjectCSV.selectByTerm( JBDate.getFYString(stdDate))

    print(console.date(lastMonth));
    for project in projects:
        showGrade = project.grade
        errors = ""
        if project.forcast_profit != project.plan_profit :
            errors += " 計画PJ粗利:"
            if project.plan_profit :
                errors += console.money(project.plan_profit)
        if project.grade == '受注':
            if project.forcast_profit > project.oper_profit :
                errors += " 実見PJ粗利:"
                if project.oper_profit :
                    errors += console.money(project.oper_profit)
            if project.forcast_month <= lastMonth :
                showGrade = "売上"
                if project.oper_profit != project.last_profit :
                    errors += " 実積売上:"
                    if project.last_profit :
                        errors += console.money(project.last_profit)
        if errors:
            try :
                print(
                    #                project.id + " " +
                    console.longTitle(project.title,30) +
                    console.left(showGrade,5) +
                    console.date(project.forcast_month) +
                    " 営業PJ粗利:" +
                    console.money(project.forcast_profit) +
                    errors )
            except Exception:
                print( console.longTitle(project.title,30) + "  ERROR "+project.id)

if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')

    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-d: DateDir")
        exit(0)

    checkProject( config, JBBot.getOption('-d') )

