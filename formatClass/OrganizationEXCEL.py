# -*- coding: utf-8 -*-
import os,csv
import pathlib
import openpyxl
import datetime
import re
from collections import namedtuple
from utility import JBBot, JBDate, console
from formatClass.CSVFormatBase import CSVFormatBase

class JobTitleLank():
    HB = 'HB'
    SB = 'SB'
    JB = 'JB'
    BU = 'BU'
    GL = 'GL'

class OrganizationEXCELError(Exception):
    pass

class MemberRow :
    pass

class OrganizationEXCEL(CSVFormatBase):
    formats = None
    base_name = None
    patch = {}
    divisions = []
    
    members = []
    Struct = namedtuple('Struct', ['division','section','group'])
    
    @staticmethod
    def setup(config,base_dir, orgmode=False):
        classfile = __file__
        classname = __class__.__name__
        (OrganizationEXCEL.formats, OrganizationEXCEL.header_rows) = CSVFormatBase.readFormat(classfile,classname)
        (OrganizationEXCEL.base_name,) = JBBot.readParam(classfile, classname, 'file',('base_name',))
        parser = JBBot.getConfigParser(classfile, classname)
        for key in parser['patch']:
            OrganizationEXCEL.patch[key] = parser.get('patch', key)

        # read data from excel file
        orgdir = config.get('data', 'base_dir')+"/"+base_dir;
        target = OrganizationEXCEL.selectFile( orgdir )
        OrganizationEXCEL.readFile( orgdir + '/' + target.file, orgmode)
        return target.date
        
    def selectFile( dirname, dateString = None ) :
        OrgEXCEL = namedtuple('OrgEXCEL', ['date','file'])
        files = []

        dir = pathlib.Path(dirname)
        for file in dir.iterdir() :
            if file.suffix == '.xlsx':
                result = re.match('(\d+)'+OrganizationEXCEL.base_name, file.name)
                if result :
                    files.append(OrgEXCEL(result.group(1),file.name))
        
        files.sort(key=lambda x:x.date, reverse=True)
        return files[0] if files else None
        
    def readFile( excel_filename, orgmode ) :
        workbook = openpyxl.load_workbook(filename=excel_filename)
        worksheet = workbook.worksheets[0]
        Member = namedtuple('Member', ['name','jobtitle', 'division', 'section', 'group','kenmu','lank','cost_code','charge'])

        struct = OrganizationEXCEL.Struct( None, None, None )
        count = 0
        for i, row in enumerate(worksheet.rows) :
            member = MemberRow()
            if not OrganizationEXCEL.formats:
                OrganizationEXCEL.setup()
            for key, format in OrganizationEXCEL.formats.items():
                data = row[format.index]
                setattr(member, key, CSVFormatBase.convert(data.value,format))
            if member.organization_name and member.organization_name[0] != '=':
                struct = OrganizationEXCEL.checkOrganization( worksheet, i,
                                                              member.organization_name,
                                                              struct);
            if member.member_count:
                count = member.member_count
                
            kenmu = False                
            name = None
            jobtitle = None
            lank = None
            cost_code = member.special_code if member.special_code and len(member.special_code) == 4 else None
            if member.director_name :
                name = member.director_name
                jobtitle = member.director_title
                if '事業部長' in jobtitle :
                    lank = JobTitleLank.JB
                elif '事業本部長' in jobtitle :
                    lank = JobTitleLank.HB
                elif '営業本部長' in jobtitle :
                    lank = JobTitleLank.SB
                else:
                    lank = JobTitleLank.BU
            elif member.manager_name :
                name = member.manager_name
                jobtitle = member.manager_title
                lank = JobTitleLank.GL
                if member.organization_name and member.organization_name[0] != '=' and jobtitle :
                    jobtitle = member.organization_name
            elif member.leader_name :
                name = member.leader_name
                if member.leader_kenmu and '兼務' in member.leader_kenmu : kenmu = True
            elif member.member_name :
                name = member.member_name
                if member.member_kenmu and '兼務' in member.member_kenmu : kenmu = True

            if jobtitle and '（兼）' in jobtitle :
                kenmu = True

            if name and ( not kenmu or orgmode ):
                if member.member_count != 0 or orgmode:
                    OrganizationEXCEL.members.append(Member(name,jobtitle,struct.division,struct.section,struct.group,kenmu,lank,cost_code,member.target_job))
                if count > 0 :
                    count = count - 1
                    if not struct.division in OrganizationEXCEL.divisions:
                        OrganizationEXCEL.divisions.append(struct.division)

    def getNamedDict(division = None) :
        members = {}
        for member in OrganizationEXCEL.members:
            if member.kenmu : continue
            if division and member.division is not division : continue
            stripname = JBBot.removeSpace(member.name)
            members[stripname] = member
        return members
            
    def getDivisionMember(division, withoutKenmu = True) :
        members = []
        for member in OrganizationEXCEL.members:
            if withoutKenmu and member.kenmu : continue
            if member.division == division :
                members.append( member )
        return members

    def getDivisions() :
        return OrganizationEXCEL.divisions
                    
    def checkOrganization( worksheet, row, name, struct ) :
        if name in OrganizationEXCEL.patch:
            patch_division = OrganizationEXCEL.patch[name]
            return OrganizationEXCEL.Struct( patch_division, name, None )
        
        orgstruct = worksheet['A'+str(row+2)]
        type = None
        if orgstruct.value :
            if orgstruct.value[0] == '=' :
                if 'A' in orgstruct.value :
                    return OrganizationEXCEL.Struct( name, None, None )
                else:
                    return OrganizationEXCEL.Struct( struct.division, name, None )
            else:
                return struct
        else:
            return OrganizationEXCEL.Struct( struct.division, struct.section, name )
                
    def printAll() :
        kenmu = 0
        for member in OrganizationEXCEL.members:
            print(console.longTitle(member.name,12),end="")
            print(console.longTitle(member.jobtitle,30), end="")
            print(console.longTitle(member.division,30), end="")
            print(console.longTitle(member.section,40), end="")
            print(console.longTitle(member.group,20), end="")
            if member.kenmu :
                print('兼務', end="")
                kenmu = kenmu + 1
            print("")
        print( str(len(OrganizationEXCEL.members)-kenmu) + " 名" )
