# -*- coding: utf-8 -*-
import os,csv
import datetime
from dateutil.relativedelta import relativedelta
from collections import namedtuple
from utility import JBDate, JBBot
from formatClass.CSVFormatBase import CSVFormatBase

class ProjectCSVError(Exception):
    pass

class ProjectCSV( CSVFormatBase ):
    contracts = []
    stdDate = None
    formats = None
    header_rows = 0

    def __init__( self, rowdata ) :
        if not ProjectCSV.formats:
            ProjectCSV.setup()
        for key, format in ProjectCSV.formats.items():
            try :
                data = rowdata[format.index]
                setattr(self, key, CSVFormatBase.convert(data,format))
            except Exception:
                print(format.colname + ":" + format.title + ":" + data)
                print(rowdata)
                raise

    def termRatio( self, termstart, termend=None ) :
        allmonths = JBDate.diff_month(self.forcast_month, self.forcast_order ) + 1
        restmonths = allmonths
        
        if self.forcast_order < termstart :
            restmonths -= JBDate.diff_month(self.forcast_order, termstart)
            
        if termend and self.forcast_month > termend :
            restmonths -= JBDate.diff_month(termend, self.forcast_month)
            
        return restmonths/allmonths if allmonths > 0 else 0

    def termAmount( self, termstart, termend=None ) :
        amount = self.oper_amount if self.oper_amount else self.forcast_amount
        return float(amount)*self.termRatio(termstart,termend)

    def termProfit( self, termstart, termend=None ) :
        profit = self.oper_profit if self.oper_profit else self.forcast_profit
        return float(self.forcast_profit)*self.termRatio(termstart,termend)

    @staticmethod
    def setup(config, base_dir, date_dir = None):
        (ProjectCSV.formats, ProjectCSV.header_rows) = CSVFormatBase.readFormat(__file__, __class__.__name__)
        # read data from projects dir
        project_dir = JBBot.selectDir( config.get('data', 'base_dir') + base_dir + '/' + config.get('data', 'project_dir'), date_dir )
        ProjectCSV.stdDate = JBBot.getDateFromPath( project_dir )
        ProjectCSV.readDir( project_dir )
        return project_dir.name
        
    def readDir( dir ):
        ProjectCSV.contracts = []
        for e in dir.iterdir() :
            if not e.suffix == '.csv' : continue
            with open(e) as f :
                csvfile = csv.reader(f)
                for i, row in enumerate(csvfile) :
                    if i < ProjectCSV.header_rows : continue; # skip header row
                    ProjectCSV.contracts.append(ProjectCSV(row))

    def getStdDate():
        return ProjectCSV.stdDate

    def selectByTermForInvestment( stddate ):
        start = JBDate.getFSStartDate(stddate)
        end = JBDate.getFSEndDate(stddate)
        selected = []
        for contract in ProjectCSV.contracts :
            if contract.type in ['A','D'] : 
                if contract.forcast_month >= start and contract.forcast_month <= end:
                    selected.append(contract)
        return selected
                    
    def selectByTerm( stddate ):
        start = JBDate.getFSStartDate(stddate)
        end = JBDate.getFSEndDate(stddate)
        selected = []
        for contract in ProjectCSV.contracts :
            if contract.type in ['X','B'] : 
                if contract.forcast_month >= start and contract.forcast_month <= end:
                    selected.append(contract)
        return selected

    def selectByTermIncludeOrder( stddate ):
        start = JBDate.getFSStartDate(stddate)
        end = JBDate.getFSEndDate(stddate)
        selected = []
        for contract in ProjectCSV.contracts :
            if contract.type in ['X','B'] : 
                if contract.forcast_month >= start :
                    selected.append(contract)
        return selected

    def makeMembersDict( contracts, transfers = None ):
        Member = namedtuple('Member' , ['bushyo','name','id','cost_code','contracts'])
        members = {}
        for contract in contracts :
            if transfers and contract.project_id and contract.project_id in transfers :
                transfer = transfers[contract.project_id]
                key = contract.cost_code + "_" + transfer.tech_code
                if not key in members:
                    members[key] = Member(contract.bushyo,
                                          transfer.tech_name,
                                          transfer.tech_code,
                                          contract.cost_code,
                                          [contract])
                else:
                    members[key].contracts.append(contract)
                
            elif contract.tech_name:
                key = contract.cost_code + "_" + contract.tech_code
                if not key in members:
                    members[key] = Member(contract.bushyo,
                                          contract.tech_name,
                                          contract.tech_code,
                                          contract.cost_code,
                                          [contract])
                else:
                    members[key].contracts.append(contract)
                
            elif contract.sales_name :
                key = contract.cost_code + "_" + contract.sales_code
                if not key in members:
                    members[key] =Member(contract.bushyo,
                                         contract.sales_name,
                                         contract.sales_code,
                                         contract.cost_code,
                                         [contract])
                else:
                    members[key].contracts.append(contract)
            else:
                raise ProjectCSError("no member project id:%s " % contract.id)
        return members

    def makeProjectDict( contracts ):
        Project = namedtuple('Project' , ['name','id','cost','contracts'])
        projects = {}
        for contract in contracts :
            if contract.project_id :
                if not contract.project_id in projects:
                    projects[contract.project_id] = Project(contract.project_name,
                                                            contract.project_id,
                                                            contract.cost_code,
                                                            [contract])
                else:
                    projects[contract.project_id].contracts.append(contract)
        return projects

    @staticmethod    
    def getTermEnd( contract, fsyear ):
        start = datetime.datetime.strptime(fsyear+"/4", "%Y/%m")
        end = datetime.datetime.strptime(str(int(fsyear)+1)+"/3", "%Y/%m") + relativedelta(day=31)
        return contract.forcast_month >= start and contract.forcast_month <= end
    
    @staticmethod    
    def getAmount( contract, last=False ):
        amount = contract.forcast_amount
        # 受注前でも計画値は取れるが、精度が悪すぎる、結果この計画値が使われることはない
        if contract.plan_amount and contract.grade == '受注' and contract.project_id:
            amount = contract.plan_amount
        if contract.oper_amount :
            amount = contract.oper_amount
        if last and contract.last_amount :
            amount = contract.last_amount
        return amount
    
    @staticmethod
    def getProfit( contract, last=False ):
        profit = contract.forcast_profit
        if contract.plan_amount and contract.grade == '受注' and contract.project_id: #huck
            profit = contract.plan_profit
        if contract.oper_profit :
            profit = contract.oper_profit
        if last and contract.last_profit :
            profit = contract.last_profit
        return profit

    @staticmethod
    def getConti( contract, last=False ):
        conti = 0
        if contract.plan_profit and contract.grade == '受注' and contract.project_id: #huck
            conti = contract.plan_conti
        if contract.oper_profit : # huck
            conti = contract.oper_conti
        return conti
