# -*- coding: utf-8 -*-
import os,csv
import pathlib
import openpyxl
import datetime
import sys
import re
from enum import Enum
from collections import namedtuple
from utility import JBBot, JBDate, console
from formatClass.CSVFormatBase import CSVFormatBase

class Times(Enum):
    WORK   = 0
    LAW    = 1
    HEALTH = 2

class WorkingEXCELError(Exception):
    pass

class WorkingRow :
    pass

class WorkingEXCEL(CSVFormatBase):
    formats = None
    base_name = None
    
    works = {}
    Work = namedtuple('Work', ['conpanyID','name','rank','times','months'])

    @staticmethod
    def setup(config, base_dir):
        classfile = __file__
        classname = __class__.__name__
        (WorkingEXCEL.formats, WorkingEXCEL.header_rows) = CSVFormatBase.readFormat(classfile,classname)
        (WorkingEXCEL.base_name,) = JBBot.readParam(classfile, classname, 'file',('base_name',))
        parser = JBBot.getConfigParser(classfile, classname)

        # read data from excel file
        working_dir = config.get('data', 'base_dir') + base_dir + '/' + config.get('data', 'working_dir')
        return WorkingEXCEL.readDir( pathlib.Path(working_dir) )

    @staticmethod
    def readDir( dir ) :
        print(WorkingEXCEL.base_name)
        dates = []
        p = re.compile(WorkingEXCEL.base_name+'_(\d+)')
        for file in dir.iterdir() :
            if file.suffix == '.xlsx':
                result = p.match(file.name)
                if result :
                    dateString = result.group(1)
                    dates.append(dateString)
                    workbook = openpyxl.load_workbook(filename=file)
                    worksheet = workbook.worksheets[0]
                    for i, row in enumerate(worksheet.rows) :
                        if i < WorkingEXCEL.header_rows : continue; # skip header row
                        working = WorkingRow()
                        if not WorkingEXCEL.formats:
                            WorkingEXCEL.setup()
                        for key, format in WorkingEXCEL.formats.items():
                            data = row[format.index]
                            setattr(working, key, CSVFormatBase.convert(data.value,format))
                        companyID = working.companyid
                        times = None
                        months = None
                        if companyID in WorkingEXCEL.works :
                            work = WorkingEXCEL.works[companyID]
                            times = work.times
                            months = work.months
                        else :
                            months = [None]*12
                            times = [0]*3
                            WorkingEXCEL.works[companyID] = WorkingEXCEL.Work(companyID, working.name, working.rank, times , months)
                        if working.law_over == '-' : working.law_over = 0
                        if working.health_over == '-' : working.health_over = 0
                        months[int(dateString[-2:])-1] = int(working.work_time)
                        times[Times.WORK.value]   += int(working.work_time)
                        times[Times.LAW.value]    += int(working.law_over )
                        times[Times.HEALTH.value] += int(working.health_over )

        dates.sort(key=lambda x: x)
        return dates
    
                        
