# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
import mysql.connector
import traceback
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from openpyxl.utils import column_index_from_string
from openpyxl.worksheet.dimensions import ColumnDimension
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.ProjectCSV import ProjectCSV

def projectImport( base_dir ) :
    config = JBBot.setup('JBBot.ini')
    basedate = ProjectCSV.setup(config, base_dir)

    try: 
        conn = mysql.connector.connect(
            host=config.get('database', 'host'),
            port=config.get('database', 'port'),
            user=config.get('database', 'user'),
            password=config.get('database', 'password'),
            database=config.get('database', 'database'),
            auth_plugin='mysql_native_password'
        )
        db_basedate = basedate[0:4]+"-"+basedate[4:6]+"-"+basedate[6:8]
        cur = conn.cursor(dictionary=True)

        cur.execute("DELETE FROM Contract WHERE recordDate = %s", (db_basedate,))
        cur.execute("DELETE FROM Project WHERE recordDate = %s", (db_basedate,))

        for contract in ProjectCSV.contracts :
            project_id = None
            pid = contract.project_id
            cost_code = contract.cost_code
            if pid :
                pstatus = 'RU' if contract.grade == '受注' else "PL"
                cur.execute("SELECT * FROM Project WHERE pid = %s and recordDate = %s and budgetCode = %s",(pid, db_basedate, cost_code))
                rows = cur.fetchall()
                if len(rows) == 0 :
                    cur.execute("INSERT INTO Project (recordDate,pid,title,budgetCode,pmuid,status,type,start,end) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                                (db_basedate,pid,contract.project_name, cost_code, contract.tech_code, pstatus, contract.project_type,
                                 JBDate.getYearMonthFromDate(contract.project_start),
                                 JBDate.getYearMonthFromDate(contract.project_end) ))
                    project_id = cur.lastrowid
                else:
                    project_id = rows[0]['_id']
                    if rows[0]['status'] == 'PL' and pstatus == 'RU' :
                        cur.execute("UPDATE Project SET status='RU' where _id = %s", project_id);

            cur.execute("INSERT INTO Contract (recordDate,cid,type,title,grade,budgetCode,techuid,salesuid,start,end,forcast_amount,forcast_profit,amount,profit,conti,project_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                        ( db_basedate, contract.id, contract.type, contract.title, contract.grade, contract.cost_code, contract.tech_code,contract.sales_code,
                          JBDate.getYearMonthFromDate(contract.forcast_order),
                          JBDate.getYearMonthFromDate(contract.forcast_month),
                          contract.forcast_amount,
                          contract.forcast_profit,
                          ProjectCSV.getAmount(contract),
                          ProjectCSV.getProfit(contract),
                          ProjectCSV.getConti(contract),
                          project_id ))

        conn.commit()
        cur.close()
        
    except Exception as e:
        print(e)
        traceback.print_exc()
        
    finally:
        if conn is not None and conn.is_connected():
            conn.close()

    
if __name__ == '__main__':


    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        exit(0)

    baseDir = JBDate.getFYString()

    if JBBot.getOption('-b', True) :
        baseDir = JBBot.getOption('-b')

    projectImport( baseDir )
