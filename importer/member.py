# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
import mysql.connector
import traceback
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from openpyxl.utils import column_index_from_string
from openpyxl.worksheet.dimensions import ColumnDimension
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.MemberEXCEL import MemberEXCEL 

def memberImport( base_dir ) :
    config = JBBot.setup('JBBot.ini')
    MemberEXCEL.setup(config, base_dir, True)

    try: 
        conn = mysql.connector.connect(
            host=config.get('database', 'host'),
            port=config.get('database', 'port'),
            user=config.get('database', 'user'),
            password=config.get('database', 'password'),
            database=config.get('database', 'database')
        )
        cur = conn.cursor(dictionary=True)

        for member in MemberEXCEL.members :
            cur.execute("INSERT IGNORE INTO User ( userid, name, email ) VALUES (%s, %s, %s)", (member.id, member.name, member.email))
            name = JBBot.removeSpace(member.name)
            cur.execute("SELECT * FROM Member WHERE name = %s and userid is null",(name, ))
            rows = cur.fetchall()
            if len(rows) == 1 :
                cur.execute("UPDATE Member SET userid=%s WHERE name = %s and userid is null",(member.id, name ))
                
        conn.commit()
        cur.close()
        
    except Exception as e:
        print(f"Error Occurred: {e}")
        traceback.print_exc()
        
    finally:
        if conn is not None and conn.is_connected():
            conn.close()

    
if __name__ == '__main__':


    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        exit(0)

    baseDir = JBDate.getFYString()

    if JBBot.getOption('-b', True) :
        baseDir = JBBot.getOption('-b')

    memberImport( baseDir )
