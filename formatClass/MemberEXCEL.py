# -*- coding: utf-8 -*-
import os,csv
import pathlib
import openpyxl
import datetime
import re
from collections import namedtuple
from utility import JBBot, JBDate, console
from formatClass.CSVFormatBase import CSVFormatBase

class MemberEXCELError(Exception):
    pass

class MemberRow :
    pass

class MemberEXCEL(CSVFormatBase):
    formats = None
    base_name = None

    members = []
    
    @staticmethod
    def setup(config, base_dir, orgmode=False):
        classfile = __file__
        classname = __class__.__name__
        (MemberEXCEL.formats, MemberEXCEL.header_rows) = CSVFormatBase.readFormat(classfile,classname)
        (MemberEXCEL.base_name,) = JBBot.readParam(classfile, classname, 'file',('base_name',))
        parser = JBBot.getConfigParser(classfile, classname)

        # read data from excel file
        orgdir = config.get('data', 'base_dir')+"/"+base_dir;
        targets = MemberEXCEL.selectFiles( orgdir )
        for file in targets :
            MemberEXCEL.readFile( orgdir + '/' + file, orgmode)
        
    def selectFiles( dirname ) :
        files = []

        dir = pathlib.Path(dirname)
        for file in dir.iterdir() :
            if file.suffix == '.xlsx':
                result = re.match('(.+)'+MemberEXCEL.base_name, file.name)
                if result :
                    print(file.name)
                    files.append(file.name)
        return files
        
    def readFile( excel_filename, orgmode ) :
        workbook = openpyxl.load_workbook(filename=excel_filename)
        worksheet = workbook.worksheets[0]
        Member = namedtuple('Member', ['id','name','email'])

        idDict = {}

        for i, row in enumerate(worksheet.rows) :
            if i < MemberEXCEL.header_rows : continue
            member = MemberRow()
            if not MemberEXCEL.formats:
                MemberEXCEL.setup()
            for key, format in MemberEXCEL.formats.items():
                data = row[format.index]
                setattr(member, key, CSVFormatBase.convert(data.value,format))

            if member.id not in idDict :
                tmember = Member(member.id, member.name, member.email)
                idDict[member.id] = tmember
                MemberEXCEL.members.append(tmember)
                
    def printAll() :
        for member in MemberEXCEL.members:
            print(console.longTitle(member.name,10),end="")
            print(console.longTitle(member.id, 12), end="")
            print(console.longTitle(member.tel,16), end="")
            print(console.longTitle(member.email,40))

