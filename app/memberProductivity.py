# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.AssignCSV import AssignCSV
from formatClass.ProjectCSV import ProjectCSV
from formatClass.WorkingEXCEL import WorkingEXCEL
from formatClass.OrganizationEXCEL import OrganizationEXCEL as orgxls
from formatClass.BaseInfo import BaseInfo

def calcProductivity( contracts, assigns, termstart, termend, months ) :
    print(termstart)
    print(termend)
    contractDic = {}
    for contract in contracts: contractDic[contract.id] = contract
        
    results = {}
    for assign in assigns :
        assign_term = 0
        for i, term in enumerate(assign.terms) :
            if i >= months : break
            if term : assign_term += float(term) 

        pcont = None
        if assign.cont_id in results:
            pcont = results[assign.cont_id]
        else :
            if assign.cont_id in contractDic:
                contract = contractDic[assign.cont_id]
                Contract = namedtuple('Contract' , ['contract','assigns'])
                pcont = Contract(contract, {})
                results[assign.cont_id] = pcont
        if pcont:
            pcont.assigns[JBBot.removeSpace(assign.name)] = { 'amount': 0, 'profit': 0, 'assign_term': assign_term }

    for calc_contract in results.values():
        contract = calc_contract.contract
        totalamount = contract.termAmount(termstart, termend)
        totalprofit = contract.termProfit(termstart, termend)
        totalwork = 0
        for assign in calc_contract.assigns.values() :
            totalwork += assign['assign_term']
            
        for assign in calc_contract.assigns.values() :
            assign['amount'] = ( totalamount * assign['assign_term'] / totalwork ) if totalwork != 0 else 0
            assign['profit'] = ( totalprofit * assign['assign_term'] / totalwork ) if totalwork != 0 else 0
            assign['totalwork'] = totalwork
    return results

def memberProductivity( config, base_dir, excel ) :
    orgxls.setup(config,base_dir)
    BaseInfo.setup(config, base_dir)
    dates = WorkingEXCEL.setup(config, base_dir)

    months = len(dates)
    
    ProjectCSV.setup(config, base_dir)
    stdDate = ProjectCSV.getStdDate();
    termstart = JBDate.getFSStartDate(stdDate)
    termend = JBDate.getDateFromYearMonth(dates[len(dates)-1])
    contracts = ProjectCSV.selectByTerm(stdDate)

    AssignCSV.setup(config, base_dir)
    assignDate = AssignCSV.getStdDate()
    assigns = AssignCSV.getAssigns()

    contractDict = calcProductivity( contracts, assigns, termstart, termend, months )

    memberAssign = AssignCSV.makeMemberNameDict(assigns)

    prefix = config.get('prefix', JBBot.appname())
    filename = prefix+'_'+'{0:%Y%m%d}'.format(stdDate)
    
    divisions = orgxls.getDivisions()
    termString = "{0:%Y/%m}".format(termstart)
    termString += ' - '
    termString += "{0:%Y/%m}".format(termend)
    termString +='('
    termString += str(months)
    termString += 'ヵ月)'

    titleCells = [
        ['D', 'Assignデータ'],
        ['F', "{0:%Y/%m/%d}".format(assignDate) ],
        ['G', 'Projectデータ'],
        ['H', "{0:%Y/%m/%d}".format(stdDate) ],
        ['J', termString ],
    ]
    
    subTitleCells = [
        ['D', '案件名', Fill.LIGHTGRAY],
        ['E', "", Fill.LIGHTGRAY],
        ['F', "", Fill.LIGHTGRAY],
        ['G', "", Fill.LIGHTGRAY],
        ['H', '期間売上', Fill.LIGHTGRAY, Align.RIGHT],
        ['I', '期間利益', Fill.LIGHTGRAY, Align.RIGHT],
        ['J', '割合', Fill.LIGHTGRAY, Align.RIGHT],
    ]

    for division in divisions :
        orgmenbers = orgxls.getDivisionMember(division)
        name = JBBot.removeSpace(orgmenbers[0].name)
        costcode = BaseInfo.getCostCode(division,name)
        if not costcode : continue
        print('making for '+ division)
        
        output = JBOutput(filename, base_dir+'_'+division, config)
        output.excelOutput(excel)
        output.headerTitles(titleCells)
        output.titleRow(division,['A','K'])

    
        # 出力ヘッダー部
        output.addHeaderCell('',13)
        cname = output.addHeaderCell('ID',8)
        output.hideCol(cname)
        cname = output.addHeaderCell('役職詳細',25)
        output.hideCol(cname)
        output.addHeaderCell('役職',4)
        output.addHeaderCell('資格',9)
        output.addHeaderCell('期間売上',16, Align.RIGHT)
        output.addHeaderCell('期間粗利',16, Align.RIGHT)
        output.addHeaderCell('利益率',10, Align.RIGHT)
        output.addHeaderCell('労働時間',10, Align.RIGHT)
        output.addHeaderCell('売上/時間',10, Align.RIGHT)
        output.addHeaderCell('平均月時間',10, Align.RIGHT)
        output.addHeaderCell('ACT',10)

        output.printHeader()
        output.headerTitles(subTitleCells)
        output.freeze('A5')

        GLRow = None
        
        section = None
        for member in orgmenbers :
            name = JBBot.removeSpace(member.name)
            if section != member.section :
                output.nextRow()
                output.titleRow(member.section,['A','F'])
                section = member.section

            if member.lank == 'GL':
                output.nextRow()
                if GLRow :
                    
                GLRow = output.rrow
                
            output.stringCell(name)

            
            detailAssigns = []
            if name in memberAssign :
                pamount = 0
                pprofit = 0
                projectassign = 0
                restassign = 0
                member_assign = memberAssign[name]
                id = member_assign.id
                output.stringCell(id)
                output.stringCell(member.jobtitle)
                output.stringCell(member.lank)
                if id in WorkingEXCEL.works : 
                     work = WorkingEXCEL.works[id]
                     output.stringCell(work.rank)
                else :
                     output.stringCell('')    
                for assign in member_assign.assigns:
                    DetailAssign = namedtuple('DetailAssign' , ['name','amount','profit','assign','total'])
                    dpamount = 0
                    dpprofit = 0
                    assign_term = 0
                    totalwork = 0
                    if assign.cont_id in contractDict :
                        contract = contractDict[assign.cont_id]
                        if name in contract.assigns :
                            contract_part = contract.assigns[name]
                            dpamount = contract_part['amount']
                            dpprofit = contract_part['profit']
                            assign_term = contract_part['assign_term']
                            totalwork = contract_part['totalwork']
                            projectassign += assign_term
                            pamount += dpamount
                            pprofit += dpprofit
                    else:
                        for i, term in enumerate(assign.terms) :
                            if i >= months : break
                            if term : restassign += float(term)
                                                
                    detailAssigns.append(DetailAssign(assign.cont_name,dpamount,dpprofit,assign_term,totalwork));

                #売上/粗利
                output.moneyCell(pamount)
                output.moneyCell(pprofit)
                if pamount != 0 :
                    ratio = pprofit / pamount
                else :
                    ratio = 0
                output.ratioCell(ratio)
                output.moneyCell(work.times[0])
                output.moneyCell(pamount/work.times[0])
                calcMonth = projectassign+restassign;
                aveWork = work.times[0]/calcMonth if calcMonth != 0 else 0
                output.moneyCell(aveWork)
                output.stringCell(str(int(projectassign+restassign))
                                  +"("+str(int(projectassign))+":"+str(int(restassign))+")")
                output.nextRow()
            else:
                output.nextRow()
            for detail in detailAssigns :
                output.skipCell(3)
                output.stringCell(detail.name, Fill.GRAY)
                output.skipCell(3, Fill.GRAY)
                output.moneyCell(detail.amount, Fill.GRAY)
                output.moneyCell(detail.profit, Fill.GRAY)
                output.ratioCalcCell(detail.assign, detail.total, Fill.GRAY)
                output.setRowDimensions(1)
                output.nextRow()

        output.save()
    
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        print("\t-e: Excel output")
        exit(0)

    if JBBot.getOption('-b', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    baseDir = JBBot.getOption('-b');
    if not baseDir:
        baseDir = JBDate.getFYString()
        
    memberProductivity( config,
                        baseDir,
                        JBBot.getOption('-e',True))
