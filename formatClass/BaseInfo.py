# -*- coding: utf-8 -*-
import os,csv
import configparser
import pathlib
import openpyxl
import datetime
import re
from collections import namedtuple
from utility import JBBot, JBDate, console
from formatClass.CSVFormatBase import CSVFormatBase
from enum import Enum

class Operation():
    DEL = '削除'
    MOD = '修正'
    TRF = '付替'

class InfoType(Enum):
    CODE = 0
    PLAN = 1
    CORR = 2

class BaseInfo():

    base_name = None
    organizations_label = None
    plans_label = None
    corrections_label = None
 
    organizations = {}
    plantitle = None
    plans = {}
    corrections = {}
    deletecodes = []
    transfers = {}
    
    @staticmethod
    def setup(config, base_dir):
        classfile = __file__
        classname = __class__.__name__
        (BaseInfo.base_name,) = JBBot.readParam(classfile, classname, 'file', ('base_name',))

        ( BaseInfo.organizations_label,
          BaseInfo.plans_label,
          BaseInfo.corrections_label ) = JBBot.readParam(classfile, classname, 'data', ('organizations','plans','corrections'));
        
        # read data from excel file
        sourcedir = config.get('data', 'base_dir')+"/"+base_dir;
        targetfile = BaseInfo.selectFile( sourcedir )
        BaseInfo.readFile( sourcedir + '/' + targetfile)

    def selectFile( dirname ) :
        BaseInfoEXCEL = namedtuple('BaseInfoEXCEL', ['date','file'])
        files = []

        dir = pathlib.Path(dirname)
        for file in dir.iterdir() :
            if file.suffix == '.xlsx':
                result = re.match(BaseInfo.base_name+'_(\d+)', file.name)
                if result :
                    files.append(BaseInfoEXCEL(result.group(1),file.name))
        
        files.sort(key=lambda x:x.date, reverse=True)
        return files[0].file if files else None

    def readFile( excel_filename ) :
        workbook = openpyxl.load_workbook(filename=excel_filename, data_only=True)
        worksheet = workbook.worksheets[0]

        index = None;
        rowdata = [[],[],[]]

        for row in worksheet.rows :
            if index != None and row[0] != None:
                rowdata[index].append(row)
            if row[0].value == BaseInfo.organizations_label :
                index = InfoType.CODE.value
            elif row[0].value == BaseInfo.plans_label :
                index = InfoType.PLAN.value
                BaseInfo.plantitle = row[1].value
            elif row[0].value == BaseInfo.corrections_label :
                index = InfoType.CORR.value
            elif row[0].value == None :
                index = None

        BaseInfo.readOrganizations(rowdata[InfoType.CODE.value])
        BaseInfo.readPlans(rowdata[InfoType.PLAN.value])
        BaseInfo.readCorrections(rowdata[InfoType.CORR.value])

    def readOrganizations( rows ) :
        CostCode = namedtuple('CostCode', ['code','mamanger','title'])
        for row in rows:
            if not row[0].value : continue
            key = row[2].value + "_" + row[1].value
            BaseInfo.organizations[key] = CostCode(row[0].value,row[1].value,row[2].value)

    def getCostCode( orgname, mgrname ) :
        costcode = None
        key = orgname+"_"+mgrname
        if key in BaseInfo.organizations :
            costcode = BaseInfo.organizations[key].code
        return costcode
        
    def readPlans( rows ) :
        PlanAmount = namedtuple('PlanAmount', ['code','amount','profit'])
        codes = rows[0]
        amounts = rows[1]
        profits = rows[2]
        for i, col in enumerate(codes) :
            BaseInfo.plans[col.value] = PlanAmount(codes[i].value,amounts[i].value,profits[i].value)
            
    def readCorrections( rows ) :
        Correction = namedtuple('Correction', ['pcode','type','amount','profit'])
        Transfer = namedtuple('Transfer', ['pcode','cost_code','tech_code','tech_name'])
        for row in rows:
            if row[1].value == Operation.DEL :
                BaseInfo.deletecodes.append(row[0].value);
            elif row[1].value == Operation.MOD :
                BaseInfo.corrections[row[0].value] = Correction(row[0].value,row[1].value,row[2].value,row[3].value)
            elif row[1].value == Operation.TRF :
                BaseInfo.transfers[row[0].value] = Transfer(row[0].value,row[2].value, row[3].value, row[4].value)
