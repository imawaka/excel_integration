#!/usr/bin/env python
# -*- coding: utf-8 -*-
import configparser
import openpyxl
import pathlib
from classMember import Member
from dateutil.parser import parse 
def is_date(string):
    try:
        parse(string)
        return True
    except:
        return False
 
config = configparser.ConfigParser()
config.read('config/checkProject.ini')

print('searching....  '+config.get('data', 'dir'))

members = {}

termstart = int(config.get('format','term_start'))
project_dir = pathlib.Path(config.get('data','dir'))

for e in project_dir.iterdir() :
    if not e.suffix == '.xlsx' : continue
    workbook = openpyxl.load_workbook(filename=e)
    worksheet = workbook.worksheets[0]

    terms = []
    for i, row in enumerate(worksheet.rows) :
        if i == 0 : # header
            for t in range(termstart, 12) :
                 datestring = "{0:%Y-%m}".format(row[t].value)
                 if not is_date(datestring) : break
                 terms.append(datestring)
        else :
            id = row[int(config.get('format','id'))].value
            if id == None : break
            member = None
            if id in members :
                member = members[id]
            else :
                name = row[int(config.get('format','name'))].value
                member = Member(name, terms)
                members[id] = member

            for t in range(termstart, 12) :
                member.setTermAsign(terms[t-termstart], row[t].value)

for m in members.values() :
    print(m.name,end="")
    m.printAsign()
    

