# -*- coding: utf-8 -*-
import os,csv
import datetime
from collections import namedtuple
from utility import JBDate, JBBot
from formatClass.CSVFormatBase import CSVFormatBase

class AssignCSVError(Exception):
    pass

class AssignCSV( CSVFormatBase ):
    allassigns = []
    stdDate = None
    formats = None
    term_start_index = None
    term_end_index = None
    termLabels = None
    
    header_rows = 0
    def __init__( self, rowdata ) :
        if not AssignCSV.formats:
            AssignCSV.setup()
        for key, format in AssignCSV.formats.items():
            try :
                data = rowdata[format.index]
                setattr(self, key,CSVFormatBase.convert(data,format))
            except Exception:
                print(format.colname + ":" + format.title + ":" + data)
                print(rowdata)
                raise
        if AssignCSV.term_start_index and AssignCSV.term_end_index :
            self.terms = []
            for index in range(AssignCSV.term_start_index,AssignCSV.term_end_index+1):
                self.terms.append(rowdata[index])
                
    @staticmethod
    def setup(config, base_dir):
        (AssignCSV.formats, AssignCSV.header_rows) = CSVFormatBase.readFormat(__file__, __class__.__name__)

        assign_dir = JBBot.selectDir( config.get('data', 'base_dir') + base_dir + '/' + config.get('data', 'assign_dir') );
        AssignCSV.stdDate = JBBot.getDateFromPath( assign_dir )
        AssignCSV.readDir(assign_dir)

    def readTermLabel(row) :
        for key, format in AssignCSV.formats.items() :
            if( format.option == 'term' ) :
                if key == 'term_start' :
                    AssignCSV.term_start_index = format.index
                elif key == 'term_end' :
                    AssignCSV.term_end_index = format.index
        if AssignCSV.termLabels :
            label_index = 0
            for index in range(AssignCSV.term_start_index, AssignCSV.term_end_index+1) :
                if row[index] != AssignCSV.termLabels[label_index] :
                    raise AssignCSVError("Error: Term mismatch ")
                label_index += 1
        else :
            AssignCSV.termLabels = []
            if AssignCSV.term_start_index and AssignCSV.term_end_index :
                for index in range(AssignCSV.term_start_index, AssignCSV.term_end_index+1) :
                    AssignCSV.termLabels.append(row[index])

    def readDir( dir ):
        AssignCSV.allassigns = []
        for e in dir.iterdir() :
            if not e.suffix == '.csv' : continue
            with open(e) as f :
                csvfile = csv.reader(f)
                for i, row in enumerate(csvfile) :
                    if i < AssignCSV.header_rows :
                        if i == AssignCSV.header_rows -1 :
                            AssignCSV.readTermLabel(row)
                        continue; # skip header row
                    AssignCSV.allassigns.append(AssignCSV(row))

    def getStdDate():
        return AssignCSV.stdDate

    def getAssigns() :
        return AssignCSV.allassigns;
                                        
    def makeMemberIDDict( assigns ) :
        Member = namedtuple('Member', ['id','name','kubun','assigns'])
        members = {}
        for assign in assigns :
            if assign.grade == '削除' : continue
            if not assign.id in members:
                members[assign.id] = Member( assign.id,
                                             assign.name,
                                             assign.kubun,
                                             [assign] )
            else :
                members[assign.id].assigns.append(assign)
                
        return members
    
    def makeMemberNameDict( assigns ) :
        Member = namedtuple('Member', ['id','name','kubun','assigns'])
        members = {}
        for assign in assigns :
            if assign.grade == '削除' : continue
            name = JBBot.removeSpace(assign.name)
            if not name in members:
                members[name] = Member( assign.id,
                                        assign.name,
                                        assign.kubun,
                                        [assign] )
            else :
                members[name].assigns.append(assign)
                
        return members
    
    def makeAssignDict( assings ):
        Project = namedtuple('Project' , ['id','pid','grade','assigns'])
        projects = {}
        for assign in assigns :
            if not assign in projects:
                projects[assign.cont_id] = Project(assign.cont_id,
                                                   assign.cont_name,
                                                   assign.proj_id,
                                                   assign.grade,
                                                   [assign])
            else:
                projects[assign.cont_id].assigns.append(assign)

        return projects
    
