# -*- coding: utf-8 -*-

from collections import namedtuple
from utility import JBBot, JBDate, console
from itertools import groupby
from formatClass.ProjectCSV import ProjectCSV
from formatClass.AssignCSV import AssignCSV


def memberAssign( config, project_dir, assign_dir ) :
    ProjectCSV.setup()
    AssignCSV.setup()

    if not project_dir : project_dir = config.get('data', 'project_dir')
    if not assign_dir : assign_dir = config.get('data', 'assign_dir')

    project_dir = JBBot.selectDir( config.get('data', 'base_dir') + project_dir );
    assign_dir = JBBot.selectDir( config.get('data', 'base_dir') + assign_dir );

    stdDate = JBBot.getDateFromPath(project_dir)
    
    projects = ProjectCSV.readDir(project_dir)
    projects = ProjectCSV.selectByTerm(projects, JBDate.getFYString(stdDate))
    
    assigns = AssignCSV.readDir(assign_dir)

    members = ProjectCSV.makeMembersDict(projects)

    termstart = JBDate.getFSDate(stdDate)

    TopMember = namedtuple('TopMember' , ['name','cost','amount','term_amount','ac_amount','d_amount'])
    topmembers = []
    for member in members.values() :
        amount = 0
        term_amount = 0
        ac_amount = 0
        d_amount = 0
        for project in member.projects:
            if project.forcast_amount:
                amount += project.forcast_amount
                term_amount += ProjectCSV.afterAmount(project,termstart)
                if project.grade in ['受注','A','B','C'] :
                    ac_amount += project.forcast_amount
                else :
                    d_amount += project.forcast_amount

        topmembers.append( TopMember(member.name, member.cost, amount, term_amount, ac_amount, d_amount) );

    topmembers.sort(key=lambda x:x.amount, reverse=True)
    for member in topmembers :
        print(console.left(member.name,12), end="")
        print(console.money(member.amount), end="")
        print(":",end="")
        print(console.money(member.term_amount), end="")
        print("(",end="")
        print(console.money(member.ac_amount), end="")
        print(":",end="")
        print(console.money(member.d_amount), end="")
        print(")")


    topmembers.sort(key=lambda x:x.cost)
    for key, group in groupby(topmembers, key=lambda m: m.cost):
        print("原価部門:", key) 
        for member in group:
            print(" "*8, end="");
            print(console.left(member.name,12), end="")
            print(console.money(member.amount))
        
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-p: PorjectDir")
        print("\t-a: AssignDir")
        exit(0)

    if JBBot.getOption('-p', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    if JBBot.getOption('-a', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)
        
    memberAssign( config,
                  JBBot.getOption('-p'),
                  JBBot.getOption('-a'))
