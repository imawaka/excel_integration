#!/usr/bin/env python
# -*- coding: utf-8 -*-
import openpyxl

meibo = openpyxl.load_workbook(filename='data/市場系部員情報.xlsx')

meibo = meibo.worksheets[0]

members = []

for i, row in enumerate(meibo.rows) :
    if i < 3 : continue
    member = [None for _ in range(5)]
    if row[0].value == "B": member[0] = 1
    elif row[0].value == "G": member[0] = 2
    elif row[0].value == "M": member[0] = 3
    elif row[0].value == None : member[0] = 4


    if row[3].value != None : member[1] = row[3].value.strip() # companyid
    if( row[4].value == None ) : continue
    
    member[2] = row[4].value.strip() # name
    if row[11].value != None : member[3] = row[11].value.strip() # email
    if row[14].value != None : member[4] = row[14].value.strip() # tel
    members.append(member)

members.sort(key=lambda x: (x[0], x[1]) )

shinsei = openpyxl.load_workbook(filename='data/smartphone_request_yyyymmdd.xlsx')
sws = shinsei.worksheets[2]
offset = 4

for i, member in enumerate(members) :
    print( member[1], member[2] )
    sws.cell(row=i+offset, column=2).value = member[2]
    sws.cell(row=i+offset, column=3).value = member[4]
    sws.cell(row=i+offset, column=4).value = i
    sws.cell(row=i+offset, column=5).value = member[1]

shinsei.save('申請用紙.xlsx')

# 終了
shinsei.close()

