# -*- coding: utf-8 -*-
import os
import sys
import re
import locale
import datetime
import configparser
import pathlib

class JBBotError(Exception):
    pass
#
# システムデフォルトのコンフィグファイルを読み込んだ後
# ホームディレクトリ配下のconf下の同名のファイルの内容で上書きする。
#
def setup(filename):
    locale.setlocale( locale.LC_ALL, 'ja_JP.UTF-8' )
    
    config = configparser.ConfigParser()
    
    jbbothome = os.environ['JBBOTHOME']
    if not jbbothome:
        raise JBBotError('env JBBOTHOME must be set!')
    config.read(jbbothome + '/config/' + filename)
    
    myconfig=os.environ['HOME'] + "/.config/"+filename
    if os.path.exists(myconfig):
        config.read(myconfig)

    return config

#
# 第２引数のディレクトリ名が指定されれば、それを返す。
# なければ、第1引数ディレクトリ配下の日付ディレクトリの最新のものを返す。
#
def selectDir( project_dir, argdir = None):
    
    print('searching project dir .... ' + project_dir)

    selectdir = None
        
    project_dir = pathlib.Path(project_dir)
    dirs = []
    for e in project_dir.iterdir() :
        if e.is_dir() :
            if argdir:
                if e.name == argdir:
                    selectdir = e
            else :
                dirs.append(e)

    if selectdir : return selectdir
                
    if not dirs :
        print('No target dir');
        exit();

    dirs.sort(key=lambda x: x.name, reverse=True)

    print('selected: '+dirs[0].name)
        
    return dirs[0]

def getArg( index ):
    argparam = None
    args = sys.argv
    if(len(args) > index) :
        argparam = args[index]
    return argparam

def getOption( option, unary = False ):
    argoption = None
    find = False
    for arg in sys.argv :
        if find :
            argoption = arg
            break
        if arg == option :
            find = True
    if find and not argoption :
        if unary :
            argoption = True
        else :
            raise JBBotError("can't find option value for "+option)

    return argoption

def getConfigParser( classfile, name ) :
    parser = configparser.ConfigParser(inline_comment_prefixes=('#',';'))
    config_file = re.sub('\.py$', '.ini', classfile)
    if not os.path.exists(config_file):
        raise JBBotError("can't find format config file for %s " % name)
    parser.read(config_file)
    return parser

def removeSpace( str ) :
    str = str.replace(' ', '')
    return str.replace('　', '')

def readParam(classfile, classname, group, params) :
    parser = getConfigParser(classfile, classname)
    values = []
    for param in params:
        values.append(parser.get(group,param))

    return tuple(values)

def printDir( dir ) :
    pathdir = pathlib.Path(dir)

    for e in pathdir.iterdir() :
        print("    "+e.name)

def targetFiles( prefix, config ) :
    output_dir = config.get('result', 'output_dir')
    type_dir = config.get('result', appname())
    if type_dir :
        output_dir = output_dir + '/' + type_dir
    pathdir = pathlib.Path(output_dir)
    targets = []
    paths = sorted(pathdir.iterdir(), key=os.path.getmtime,reverse=True)
    for e in paths :
        if e.name.find(prefix) == 0 :
            targets.append(e.name)
    return targets

def appname() :
    return sys.argv[0].replace('.py','')

def selection( candidates ):
    for i, cand in enumerate(candidates):
        print(str(i+1) + ". "+cand)
    num = input('Select: ');
    return candidates[int(num)-1]

#
# Path Object から日付を取得
#
def getDateFromPath( project_dir ):
    return datetime.datetime.strptime(project_dir.name, "%Y%m%d")




