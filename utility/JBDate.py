# -*- coding: utf-8 -*-
import datetime
from dateutil.relativedelta import relativedelta
#
# 
#
def getDate( dateString = None ):
    return datetime.datetime.strptime(dateString, "%Y/%m/%d") if dateString else datetime.datetime.now()
#
# 年度初の年の文字列
#
def getFYString(stdDate = None):
    dt_now = stdDate if stdDate else datetime.datetime.now() 
    year = dt_now.year
    if dt_now.month < 4:
        year -= 1
    return str(year)
#
# 年度初の日付
#
def getFSStartDate(stddate = None):
    fsyear = getFYString(stddate)
    return datetime.datetime.strptime(str(int(fsyear))+"/4", "%Y/%m")
#
# 年度末の日付
#
def getFSEndDate(stddate = None):
    fsyear = getFYString(stddate)
    return datetime.datetime.strptime(str(int(fsyear)+1)+"/3", "%Y/%m") + relativedelta(day=31)
#
# 前月初
#
def getLastMonth( today ) :
    thismonth = datetime.datetime(today.year, today.month, 1)
    return thismonth + datetime.timedelta(days=-1)
#
# 日付
#
def getDateFromYearMonth( yearmonth ):
    return datetime.datetime.strptime(yearmonth, "%Y%m")

def diff_month(d1, d2):
    if d1 > d2:
        d1, d2 = d2, d1
    return (d2.year - d1.year)*12 + d2.month - d1.month

#
# 文字列
#
def getYearMonthFromDate( date ):
    year = str(date.year)
    month = str(date.month).zfill(2)
    return year+month

