# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
import mysql.connector
import traceback
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from openpyxl.utils import column_index_from_string
from openpyxl.worksheet.dimensions import ColumnDimension
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.OrganizationEXCEL import OrganizationEXCEL as orgxls
from formatClass.OrganizationEXCEL import JobTitleLank

def findLeader( members, start, ranks ) :
    leader = None;
    for member in members[start::] :
        if member.lank in ranks :
            leader = JBBot.removeSpace(member.name)
            break
    return leader

def organizationImport( base_dir ) :
    config = JBBot.setup('JBBot.ini')
    basedate = orgxls.setup(config, base_dir, True)

    try: 
        conn = mysql.connector.connect(
            host=config.get('database', 'host'),
            port=config.get('database', 'port'),
            user=config.get('database', 'user'),
            password=config.get('database', 'password'),
            database=config.get('database', 'database')
        )
        db_basedate = basedate[0:4]+"-"+basedate[4:6]+"-"+basedate[6:8]
        cur = conn.cursor(dictionary=True)

        cur.execute("DELETE FROM OrganizationMember WHERE effectiveDate = %s ", (db_basedate,))
        cur.execute("DELETE FROM Organization WHERE effectiveDate = %s ORDER BY _id DESC", (db_basedate,))

        userDict = {}
        cur.execute("SELECT userid, name FROM User");
        users = cur.fetchall()
        for user in users :
            name = JBBot.removeSpace(user['name'])
            userDict[name] = user['userid']

        defaultDict = {}
        cur.execute("SELECT * FROM OrganizationDefault");
        orgdefs = cur.fetchall()
        for orgdef in orgdefs :
            defaultDict[orgdef['organization_name']] = orgdef

        for division in orgxls.getDivisions() :
            orgmenbers = orgxls.getDivisionMember( division, False )
            leader = findLeader(orgmenbers,0, ['JB','HB','SB'])
            budgetC = None
            budgetM = 0
            if division in defaultDict :
                map = defaultDict[division]
                budgetC = map['budgetCode']
                budgetM = map['management']
                leader_default = map['leader_name']
                if leader_default :
                    leader = JBBot.removeSpace(leader_default)
            cur.execute("INSERT INTO Organization (name, effectiveDate, leader, budgetCode, budgetManagement) VALUES (%s, %s, %s, %s, %s)"
                        , (division, db_basedate, leader, budgetC, budgetM))
            division_id = cur.lastrowid
            my_org = division_id

            section = None
            section_id = None
            group = None
            group_id = None
            order = 0
            for i, member in enumerate(orgmenbers) :
                if member.section and member.section.strip() != "":
                    if section != member.section.strip() :
                        order = 0
                        group = None
                        group_id = None
                        section = member.section.strip()
                        mapkey = division+" "+section
                        budgetC = None
                        budgetM = 0
                        leader = findLeader(orgmenbers,i, ['BU'])
                        if mapkey in defaultDict :
                            map = defaultDict[mapkey]
                            budgetC = map['budgetCode']
                            budgetM = map['management']
                            leader_default = map['leader_name']
                            if leader_default :
                                leader = JBBot.removeSpace(leader_default)
                        cur.execute("INSERT INTO Organization (name, effectiveDate, parent_id, leader, budgetCode, budgetManagement) VALUES (%s, %s, %s, %s, %s, %s)"
                                    , (section, db_basedate, division_id, leader, budgetC, budgetM))
                        section_id = cur.lastrowid
                    my_org = section_id
                if member.group and member.group.strip() != "":
                    if group != member.group.strip() :
                        order = 0
                        pid = section_id
                        if not pid : pid = division_id
                        group = member.group.strip()
                        mapkey = division
                        if section : mapkey = mapkey+ " "+section
                        mapkey = mapkey +" "+group
                        leader = findLeader(orgmenbers,i, ['GL'])
                        if mapkey in defaultDict :
                            map = defaultDict[mapkey]
                            leader_default = map['leader_name']
                            if leader_default :
                                leader = JBBot.removeSpace(leader_default)
                        cur.execute("INSERT INTO Organization (name, effectiveDate, parent_id, leader) VALUES (%s, %s, %s, %s)", (group, db_basedate, pid, leader))
                        group_id = cur.lastrowid
                    my_org = group_id

                name_nsp = JBBot.removeSpace(member.name)
                cur.execute("SELECT * FROM Member WHERE name = %s",(name_nsp, ))
                rows = cur.fetchall()
                member_id = None
                if len(rows) == 0 :
                    uid = None
                    if name_nsp in userDict :
                        uid = userDict[name_nsp]
                    cur.execute("INSERT INTO Member (name, userid) VALUES (%s, %s)", (name_nsp, uid))
                    member_id = cur.lastrowid
                elif len(rows) == 1 :
                    member_id = rows[0]['_id']
                #else :
                    # same name ??
                   
                cur.execute("INSERT INTO OrganizationMember (effectiveDate, sorder, additional, jobtitle, charge, leaderrank, member_id, organization_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (db_basedate, order, member.kenmu, member.jobtitle, member.charge, member.lank, member_id, my_org))
                order = order + 1
        conn.commit()
        cur.close()

    except Exception as e:
        print(f"Error Occurred: {e}")
        traceback.print_exc()
        
    finally:
        if conn is not None and conn.is_connected():
            conn.close()

    
if __name__ == '__main__':

    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        exit(0)

    baseDir = JBDate.getFYString()

    if JBBot.getOption('-b', True) :
        baseDir = JBBot.getOption('-b')

    organizationImport( baseDir )
