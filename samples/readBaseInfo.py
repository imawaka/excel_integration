# -*- coding: utf-8 -*-
from utility import JBBot, JBDate, console
from formatClass.BaseInfo import BaseInfo 

def readBaseInfo( config ) :
    baseDir = JBDate.getFYString()
    BaseInfo.setup(config, baseDir)

    print(BaseInfo.organizations)
    print("");
    print(BaseInfo.plans)
    print("");
    print(BaseInfo.deletecodes)
    print("");
    print(BaseInfo.corrections)

if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    readBaseInfo( config )

