# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
import mysql.connector
import traceback
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from openpyxl.utils import column_index_from_string
from openpyxl.worksheet.dimensions import ColumnDimension
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.AssignCSV import AssignCSV

def assignImport( base_dir ) :
    config = JBBot.setup('JBBot.ini')
    AssignCSV.setup(config, base_dir)

    try: 
        conn = mysql.connector.connect(
            host=config.get('database', 'host'),
            port=config.get('database', 'port'),
            user=config.get('database', 'user'),
            password=config.get('database', 'password'),
            database=config.get('database', 'database'),
	    auth_plugin='mysql_native_password'
        )
        cur = conn.cursor(dictionary=True)

        cur.execute("DELETE FROM Assign WHERE yearMonth > %s", (base_dir+"04",))

        terms = [];
        for label in AssignCSV.termLabels:
            label = re.sub("^'","", label)
            label = re.sub("/","", label)
            terms.append(label)
        for assign in AssignCSV.getAssigns() :
            for i, term in enumerate(assign.terms) :
                if term == '':  term = None
                cur.execute("INSERT INTO Assign (contractid,userid,yearMonth,assign) VALUES (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE "\
                            "contractid = %s, userid = %s, yearMonth = %s, assign = %s",
                            (assign.cont_id, assign.id, terms[i], term, assign.cont_id, assign.id, terms[i], term))
            
        conn.commit()
        cur.close()
        
    except Exception as e:
        print(f"Error Occurred: {e}")
        traceback.print_exc()
        
    finally:
        if conn is not None and conn.is_connected():
            conn.close()

    
if __name__ == '__main__':

    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        exit(0)

    baseDir = JBDate.getFYString()

    assignImport( baseDir )
