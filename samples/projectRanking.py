# -*- coding: utf-8 -*-

from collections import namedtuple
from utility import JBBot, JBDate, console
from itertools import groupby
from formatClass.ProjectCSV import ProjectCSV


def projectRanking( config, project_dir, date_dir, excel ) :
    ProjectCSV.setup()

    if not project_dir : project_dir = config.get('data', 'project_dir')

    project_dir = JBBot.selectDir( config.get('data', 'base_dir') +
                                   project_dir,
                                   date_dir)

    stdDate = JBBot.getDateFromPath(project_dir)
    projects = ProjectCSV.readDir(project_dir)

    projects = ProjectCSV.selectByTerm(projects, JBDate.getFYString(stdDate))

    pprojects = ProjectCSV.makeProjectDict(projects)

    termstart = JBDate.getFSDate(stdDate)

    TopProject = namedtuple('TopProject' , ['name','cost','amount','ac_amount','d_amount'])
    topprojects = []
    for pproject in pprojects.values() :
        amount = 0
        ac_amount = 0
        d_amount = 0
        for project in pproject.projects:
            if project.forcast_amount:
                amount += project.forcast_amount
                if project.grade in ['受注','A','B','C'] :
                    ac_amount += project.forcast_amount
                else :
                    d_amount += project.forcast_amount

        topprojects.append( TopProject(pproject.name, pproject.cost, amount, ac_amount, d_amount) );

    topprojects.sort(key=lambda x:x.amount, reverse=True)
    for project in topprojects :
        print(console.longTitle(project.name,30), end="")
        print(console.money(project.amount), end="")
        print("(",end="")
        print(console.money(project.ac_amount), end="")
        print(":",end="")
        print(console.money(project.d_amount), end="")
        print(")")

    topprojects.sort(key=lambda x:x.cost)
    for key, group in groupby(topprojects, key=lambda m: m.cost):
        print("原価部門:", key) 
        for project in group:
            print(" "*8, end="");
            print(console.longTitle(project.name,30), end="")
            print(console.money(project.amount))

if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-p: PorjectDir")
        print("\t-d: DateDir")
        print("\t-e: Excel output")
        exit(0)

    if JBBot.getOption('-p', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    if JBBot.getOption('-d', True) == True :
        basedir = config.get('data', 'base_dir')
        projectdir = JBBot.getOption('-p')
        if not projectdir :
            projectdir = config.get('data', 'project_dir') 
        JBBot.printDir(basedir+projectdir)
        exit(0)
        
        
    projectRanking( config,
                    JBBot.getOption('-p'),
                    JBBot.getOption('-d'),
                    JBBot.getOption('-e',True))
