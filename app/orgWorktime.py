# -*- coding: utf-8 -*-
import re
import numpy
import openpyxl
from itertools import chain
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.AssignCSV import AssignCSV
from formatClass.ProjectCSV import ProjectCSV
from formatClass.WorkingEXCEL import WorkingEXCEL
from formatClass.OrganizationEXCEL import OrganizationEXCEL as orgxls
from formatClass.BaseInfo import BaseInfo

class WorkMember :

    def __init__( self, member ) :
        self.id = ''
        self.work = None
        self.member = member
        self.assigns = []

    def setID( self, id ) :
        self.id = id

    def addAssign( self, assign ) :
        self.assigns.append(assign)

    def setWork( self, work ) :
        self.work = work

    def getRank( self ) :
        return self.work.rank if self.work else ""

    def getMonthWork(self, month):
        monthWork = None
        if self.work :
            monthWork = self.work.months[month-1]
        return monthWork
    
    def average( self, months ) :
        wmonth = 0 
        aveWork = 0
        if self.work :
            for month in months :
                work = self.work.months[month-1]
                if work :
                    aveWork += work
                    wmonth += 1
        if wmonth > 0 :
            aveWork = int(aveWork/wmonth)
        return aveWork
    
    
class WorkGroup :
    def __init__( self, name, months ) :
        self.name = name
        self.months = months
        self.total = [] 
        self.members = []
        self.subgroup = []
        self.total = [] * ( len(months) + 1 )

    def addMember( self, member ) :
        workmember = WorkMember(member)
        self.members.append(workmember)
        return workmember

    def addGroup( self, group ) :
        self.subgroup.append(group)

    def average( self ) :
        memnum = len(self.members)
        aveWork = 0
        for workmember in self.members :
            aveWork += workmember.average(self.months)
        for group in self.subgroup :
            groupAve = group.average();
            aveWork += groupAve[0]*groupAve[1]
            memnum += groupAve[1]
        if memnum > 0 :
            aveWork = int(aveWork/memnum)
        
        return [aveWork,memnum]

    def monthAverage( self, month ) :
        memnum = 0
        aveWork = 0
        for workmember in self.members :
            monthWork = workmember.getMonthWork(month)
            if monthWork :
                memnum += 1
                aveWork += monthWork
        for group in self.subgroup :
            groupAve = group.monthAverage(month);
            aveWork += groupAve[0]*groupAve[1]
            memnum += groupAve[1]

        if memnum > 0:
            aveWork = aveWork/memnum
        return [aveWork, memnum]

    def cellBG(self, value):
        color = Fill.LIGHTYELLOW
        if value > 200 :
            color = Fill.RED
        elif value > 180 :
            color = Fill.YELLOW
        return color

    def averagePrint( self, output ) :
        work = self.average()[0]
        output.moneyCell(work, self.cellBG(work))
        for month in self.months :
            aveWork = self.monthAverage(month)
            work = aveWork[0]
            output.moneyCell(work, self.cellBG(work))

    def outputMember( self, output ) :
        for workmember in self.members :
            member = workmember.member
            output.skipCell(1)
            output.stringCell(member.name)
            output.stringCell(workmember.id)
            output.stringCell(member.lank)
            output.stringCell(workmember.getRank())

            output.moneyCell(workmember.average(self.months))

            for month in self.months :
                if workmember.work :
                    output.moneyCell(workmember.work.months[month-1])
            output.nextRow()
            for detail in workmember.assigns :
                if output.excel :
                    output.skipCell(3)
                    output.stringCell(detail.name, Fill.GRAY)
                    output.skipCell(6, Fill.GRAY)
                    output.moneyCell(detail.amount, Fill.GRAY)
                    output.moneyCell(detail.profit, Fill.GRAY)
                    output.ratioCalcCell(detail.assign, detail.total, Fill.GRAY)
                    output.setRowDimensions(1)
                    output.nextRow()
                
def calcProductivity( contracts, assigns, termstart, termend, months ) :
    print(termstart)
    print(termend)
    contractDic = {}
    for contract in contracts: contractDic[contract.id] = contract
        
    results = {}
    for assign in assigns :
        assign_term = 0
        for i, term in enumerate(assign.terms) :
            if i >= months : break
            if term : assign_term += float(term) 

        pcont = None
        if assign.cont_id in results:
            pcont = results[assign.cont_id]
        else :
            if assign.cont_id in contractDic:
                contract = contractDic[assign.cont_id]
                Contract = namedtuple('Contract' , ['contract','assigns'])
                pcont = Contract(contract, {})
                results[assign.cont_id] = pcont
        if pcont:
            pcont.assigns[JBBot.removeSpace(assign.name)] = { 'amount': 0, 'profit': 0, 'assign_term': assign_term }

    for calc_contract in results.values():
        contract = calc_contract.contract
        totalamount = contract.termAmount(termstart, termend)
        totalprofit = contract.termProfit(termstart, termend)
        totalwork = 0
        for assign in calc_contract.assigns.values() :
            totalwork += assign['assign_term']
            
        for assign in calc_contract.assigns.values() :
            assign['amount'] = ( totalamount * assign['assign_term'] / totalwork ) if totalwork != 0 else 0
            assign['profit'] = ( totalprofit * assign['assign_term'] / totalwork ) if totalwork != 0 else 0
            assign['totalwork'] = totalwork
    return results

def orgWorktime( config, base_dir, excel ) :
    orgxls.setup(config,base_dir)
    BaseInfo.setup(config, base_dir)
    dates = WorkingEXCEL.setup(config, base_dir)
    months = []
    for date in dates :
        months.append(int(date[-2:]));

    ProjectCSV.setup(config, base_dir)
    stdDate = ProjectCSV.getStdDate();
    termstart = JBDate.getFSStartDate(stdDate)
    termend = JBDate.getDateFromYearMonth(dates[len(dates)-1])
    stdDate = ProjectCSV.getStdDate();
    contracts = ProjectCSV.selectByTerm(stdDate)

    AssignCSV.setup(config, base_dir)
    assignDate = AssignCSV.getStdDate()
    assigns = AssignCSV.getAssigns()

    contractDict = calcProductivity( contracts, assigns, termstart, termend, len(months) )

    memberAssign = AssignCSV.makeMemberNameDict(assigns)

    prefix = config.get('prefix', JBBot.appname())
    filename = prefix+'_'+'{0:%Y%m%d}'.format(stdDate)
    
    divisions = orgxls.getDivisions()


    for division in divisions :
        orgmenbers = orgxls.getDivisionMember(division)
        name = JBBot.removeSpace(orgmenbers[0].name)
        costcode = BaseInfo.getCostCode(division,name)
        if not costcode : continue
        print('making for '+ division)
        
        groupstruct = []

        worksection = None
        workgroup = None


        
        section = None
        for member in orgmenbers :
            name = JBBot.removeSpace(member.name)
            if not workgroup:
                workgroup = WorkGroup('事業部共通', months)
                worksection = workgroup
            if section != member.section :
                section = member.section
                groupstruct.append(worksection)
                workgroup = WorkGroup(section, months)
                worksection = workgroup

            if member.lank == 'GL' :
                workgroup = WorkGroup(member.jobtitle, months)
                worksection.addGroup(workgroup)
            
            workmember = workgroup.addMember(member)
            detailAssigns = []
            if name in memberAssign :
                pamount = 0
                pprofit = 0
                projectassign = 0
                restassign = 0
                member_assign = memberAssign[name]
                id = member_assign.id
                workmember.setID(id)
                
                if id in WorkingEXCEL.works : 
                     work = WorkingEXCEL.works[id]
                     workmember.setWork(work)
                for assign in member_assign.assigns:
                    DetailAssign = namedtuple('DetailAssign' , ['name','amount','profit','assign','total'])
                    dpamount = 0
                    dpprofit = 0
                    assign_term = 0
                    totalwork = 0
                    if assign.cont_id in contractDict :
                        contract = contractDict[assign.cont_id]
                        if name in contract.assigns :
                            contract_part = contract.assigns[name]
                            dpamount = contract_part['amount']
                            dpprofit = contract_part['profit']
                            assign_term = contract_part['assign_term']
                            totalwork = contract_part['totalwork']
                            projectassign += assign_term
                            pamount += dpamount
                            pprofit += dpprofit
                    else:
                        for i, term in enumerate(assign.terms) :
                            if i >= len(months) : break
                            if term : restassign += float(term)

                    workmember.addAssign(DetailAssign(assign.cont_name, dpamount,dpprofit,assign_term,totalwork));
        groupstruct.append(worksection)


        subTitleCells = [
            ['D', '案件名', Fill.LIGHTGRAY, Align.LEFT, ['D','J']],
            ['K', '期間売上', Fill.LIGHTGRAY, Align.RIGHT],
            ['L', '期間利益', Fill.LIGHTGRAY, Align.RIGHT],
            ['M', '割合', Fill.LIGHTGRAY, Align.RIGHT],
        ]

        output = JBOutput(filename, base_dir+'_'+division, config)
        output.excelOutput(excel)

        # 出力ヘッダー部
        output.addHeaderCell('組織',13)
        output.addHeaderCell('氏名',13)
        cname = output.addHeaderCell('ID',8)
        output.hideCol(cname)
        output.addHeaderCell('役職',4)
        output.addHeaderCell('資格',9)
        output.addHeaderCell('平均月時間',10, Align.RIGHT)
        monthes = chain(range(4,13),range(1,4))
        for month in monthes :
            output.addHeaderCell(str(month)+'月',10,Align.RIGHT)

        output.printHeader()
        output.headerTitles(subTitleCells)
        output.freeze('A4')

        output.titleCell(division,['A','E'])

        memnum = 0
        aveWork = 0
        for group in groupstruct :
            groupAve = group.average();
            aveWork += groupAve[0]*groupAve[1]
            memnum += groupAve[1]
        aveWork = int(aveWork/memnum)
        output.moneyCell(aveWork,Fill.LIGHTYELLOW)

        for month in months :
            memnum = 0
            aveWork = 0
            for group in groupstruct :
                groupAve = group.monthAverage(month)
                aveWork += groupAve[0]*groupAve[1]
                memnum += groupAve[1]
            aveWork = int(aveWork/memnum)
            output.moneyCell(int(aveWork),Fill.LIGHTYELLOW)
        
        output.nextRow()
        
        for group in groupstruct :
            output.titleCell(group.name, ['A','E'])
            group.averagePrint(output)
            output.nextRow()
            group.outputMember(output)
            for subgroup in group.subgroup :
                output.stringCell(subgroup.name, Fill.LIGHTYELLOW)
                output.skipCell(4, Fill.LIGHTYELLOW)
                subgroup.averagePrint(output)
                output.nextRow()
                subgroup.outputMember(output)

        output.save()
    
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        print("\t-e: Excel output")
        exit(0)

    if JBBot.getOption('-b', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    baseDir = JBBot.getOption('-b');
    if not baseDir:
        baseDir = JBDate.getFYString()
        
    orgWorktime( config, baseDir, JBBot.getOption('-e',True))
