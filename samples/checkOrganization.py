# -*- coding: utf-8 -*-
from utility import JBBot, JBDate, console
from formatClass.OrganizationEXCEL import OrganizationEXCEL as orgxls

def checkOrganization( config ) :
    orgxls.setup(config, True)

    orgxls.printAll()

    print("");

    print(orgxls.getDivisions())

if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    checkOrganization( config )

