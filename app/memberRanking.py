# -*- coding: utf-8 -*-

from collections import namedtuple
import openpyxl
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.ProjectCSV import ProjectCSV
from formatClass.BaseInfo import BaseInfo

def memberRanking( config, base_dir, excel ) :
    ProjectCSV.setup(config, base_dir)
    BaseInfo.setup(config, base_dir)
    
    stdDate = ProjectCSV.getStdDate();
    projects = ProjectCSV.selectByTerm(stdDate)
    members = ProjectCSV.makeMembersDict(projects)

    termstart = JBDate.getFSStartDate(stdDate)

    TopMember = namedtuple('TopMember' , ['bushyo','name','cost','amount','term_amount','ac_amount','d_amount'])
    topmembers = []
    for member in members.values() :
        amount = 0
        term_amount = 0
        ac_amount = 0
        d_amount = 0
        for contract in member.contracts:
            if contract.forcast_amount:
                amount += contract.forcast_amount
                term_amount += contract.termAmount(termstart)
                if contract.grade in ['受注','A','B','C'] :
                    ac_amount += contract.forcast_amount
                else :
                    d_amount += contract.forcast_amount
        topmembers.append( TopMember(member.bushyo, member.name, member.cost_code, amount, term_amount, ac_amount, d_amount) );

    topmembers.sort(key=lambda x:x.amount, reverse=True)

    prefix = config.get('prefix', JBBot.appname())
    filename = prefix+'_'+'{0:%Y%m%d}'.format(stdDate)
    output = JBOutput(filename, 'Ranking', config)

    output.excelOutput(excel)

    output.headerRow([['A',5,''],
                      ['B',13,'氏名'],
                      ['C',13,'部署'],
                      ['D',17,'売上', Align.RIGHT],
                      ['E',17,'期中売上', Align.RIGHT],
                      ['F',17,'確度A-C', Align.RIGHT],
                      ['G',17, '確度D', Align.RIGHT]])

    rank = 0
    for member in topmembers :
        rank += 1
        output.stringCell(str(rank))
        output.stringCell(member.name)
        output.stringCell(member.bushyo[3:10])
        output.moneyCell(member.amount)
        output.moneyCell(member.term_amount)
        output.moneyCell(member.ac_amount)
        output.moneyCell(member.d_amount)
        output.nextRow()

    topmembers.sort(key=lambda x:x.cost)
    for key, group in groupby(topmembers, key=lambda m: m.cost):
        output.nextRow()
        output.titleRow("原価部門："+key, ['A','F'])
        
        for member in group:
            output.stringCell("")
            output.stringCell(member.name)
            output.moneyCell(member.amount)
            output.nextRow()
            
    output.save()
        
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        print("\t-e: Excel output")
        exit(0)

    if JBBot.getOption('-b', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    baseDir = JBBot.getOption('-b');
    if not baseDir:
        baseDir = JBDate.getFYString()
        
    memberRanking( config,
                   baseDir,
                   JBBot.getOption('-e',True))
