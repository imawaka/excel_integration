# -*- coding: utf-8 -*-

from collections import namedtuple
from utility import JBBot, JBDate, console
from itertools import groupby
from formatClass.ProjectCSV import ProjectCSV


def memberProjectRanking( config, project_dir, date_dir ) :
    ProjectCSV.setup()

    if not project_dir : project_dir = config.get('data', 'project_dir')

    project_dir = JBBot.selectDir( config.get('data', 'base_dir') +
                                   project_dir,
                                   date_dir)

    stdDate = JBBot.getDateFromPath(project_dir)
    projects = ProjectCSV.readDir(project_dir)

    projects = ProjectCSV.selectByTerm(projects, JBDate.getFYString(stdDate))

    members = ProjectCSV.makeMembersDict(projects)

    termstart = JBDate.getFSDate(stdDate)

    TopMember = namedtuple('TopMember' , ['name','cost','amount','term_amount','ac_amount','d_amount','pprojects'])
    topmembers = []
    for member in members.values() :
        amount = 0
        term_amount = 0
        ac_amount = 0
        d_amount = 0
        pprojects = {}
        for project in member.projects:
            if project.forcast_amount:
                p_amount = project.forcast_amount
                p_ac_amount = 0
                p_d_amount = 0
                if project.grade in ['受注','A','B','C'] :
                    p_ac_amount = project.forcast_amount
                else :
                    p_d_amount += project.forcast_amount
                amount += p_amount
                ac_amount += p_ac_amount
                d_amount += p_d_amount
                term_amount += ProjectCSV.afterAmount(project,termstart)
                
                if project.project_id :
                    if not project.project_id in pprojects:
                        pprojects[project.project_id] = { 'id': project.project_id,
                                                          'name': project.project_name,
                                                          'amount':    p_amount,
                                                          'ac_amount': p_ac_amount,
                                                          'd_amount':   p_d_amount }
                    else:
                        pproject = pprojects[project.project_id]
                        pproject['amount'] += p_amount
                        pproject['ac_amount'] += p_ac_amount
                        pproject['d_amount'] += p_d_amount
                else:
                    pprojects[project.id] = { 'id': project.id,
                                              'name': '[NP]'+project.title,
                                              'amount':    p_amount,
                                              'ac_amount': p_ac_amount,
                                              'd_amount':   p_d_amount }
                                            
        topmembers.append( TopMember(member.name, member.cost, amount, term_amount, ac_amount, d_amount, pprojects) );

    topmembers.sort(key=lambda x:x.amount, reverse=True)
    for member in topmembers :
        print(console.left(member.name,12), end="")
        print(console.money(member.amount), end="")
        print(":",end="")
        print(console.money(member.term_amount), end="")
        print("(",end="")
        print(console.money(member.ac_amount), end="")
        print(":",end="")
        print(console.money(member.d_amount), end="")
        print(")")
        for project in member.pprojects.values():
            print(" "*8, end="");
            print(console.longTitle(project['name'],30), end="")
            print(console.money(project['amount']), end="")
            print("(",end="")
            print(console.money(project['ac_amount']), end="")
            print(":",end="")
            print(console.money(project['d_amount']), end="")
            print(")")


'''
    topmembers.sort(key=lambda x:x.cost)
    for key, group in groupby(topmembers, key=lambda m: m.cost):
        print("原価部門:", key) 
        for member in group:
            print(" "*8, end="");
            print(console.left(member.name,12), end="")
            print(console.money(member.amount))
'''
        
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-p: PorjectDir")
        print("\t-d: DateDir")
        exit(0)

    if JBBot.getOption('-p', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    if JBBot.getOption('-d', True) == True :
        basedir = config.get('data', 'base_dir')
        projectdir = JBBot.getOption('-p')
        if not projectdir :
            projectdir = config.get('data', 'project_dir') 
        JBBot.printDir(basedir+projectdir)
        exit(0)
        
        
    memberProjectRanking( config,
                   JBBot.getOption('-p'),
                   JBBot.getOption('-d'))
