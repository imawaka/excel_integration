class Member:
    def __init__(self, name, terms) :
        self.name = name
        self.terms = {}
        self.project = {}
        for term in terms :
            self.terms[term] = 0.0

    def setTermAsign( self, key, asign ) :
        if asign :
            self.terms[key] += float(asign)

    def printAsign( self ) :
        print(self.terms)
