# -*- coding: utf-8 -*-
import re
import numpy as np
import openpyxl
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment
from collections import namedtuple
from utility import JBBot, JBDate
from utility.JBOutput import JBOutput, Align, Fill
from itertools import groupby
from formatClass.AssignCSV import AssignCSV
from formatClass.OrganizationEXCEL import OrganizationEXCEL as orgxls
from formatClass.BaseInfo import BaseInfo

def checkAssign( config, base_dir, excel, nodetail ) :
    orgxls.setup(config, base_dir)
    BaseInfo.setup(config, base_dir)
        
    AssignCSV.setup(config, base_dir)
    stdDate = AssignCSV.getStdDate()
    assigns = AssignCSV.getAssigns()
    memberAssign = AssignCSV.makeMemberNameDict(assigns)
    
    prefix = config.get('prefix', JBBot.appname())
    filename = prefix+'_'+'{0:%Y%m%d}'.format(stdDate);
    
    divisions = orgxls.getDivisions()

    titleCells = [
        ['A', 'データ採取日'],
        ['B', "{0:%Y/%m/%d}".format(stdDate) ],
    ]

    for division in divisions :
        orgmenbers = orgxls.getDivisionMember(division)
        name = JBBot.removeSpace(orgmenbers[0].name)
        costcode = BaseInfo.getCostCode(division,name)
        if not costcode : continue
        print('making for '+ division)
        
        output = JBOutput(filename, base_dir+'_'+division, config)
        output.excelOutput(excel)
        output.titleRow(division,['A','N'])
        output.headerTitles(titleCells)
        
        headerCells = [['A',13,'Name'],['B',8,'No.']]
        termsize = 0
        for termLabel in AssignCSV.termLabels:
            strlabel = re.sub("^'","",termLabel)
            headerCells.append([output.colname(termsize+3), 10, strlabel, Align.RIGHT])
            termsize += 1

        headerCells.append([output.colname(termsize+3), 20, ''])
        output.headerRow(headerCells)
        output.headerRowFix()
    
        section = None
        for member in orgmenbers :
            name = JBBot.removeSpace(member.name)
            if section != member.section :
                output.nextRow()
                output.titleRow(member.section,['A','N'])
                section = member.section
            output.stringCell(name)


            detailAssigns = []
            if name in memberAssign :
                member_assign = memberAssign[name]
                output.stringCell(member_assign.id)
                terms = np.zeros((termsize, 4))
                for assign in member_assign.assigns:
                    for i, term in enumerate(assign.terms) :
                        if term  :
                            terms[i][0] += float(term)
                            if assign.grade == 'D' :
                                terms[i][1] += float(term)
                                if assign.cont_id[:3] == 'CAC' :
                                    if 'コロナ' in assign.cont_name:
                                        terms[i][3] += float(term)
                                    else :
                                        terms[i][2] += float(term)
                    detailAssigns.append(assign);

                for term in terms:
                    termString = '{:.1f}'.format(term[0])
                    #termString = str(term[0])
                    if term[1] > 0.0 : termString += "(D:"+'{:.1f}'.format(term[1]) + ")"
                    if term[2] > 0.0 or term[3] > 0.0 : termString += "(I:"+'{:.1f}'.format(term[2]+term[3]) + ")"

                    color = None
                    if term[0] == 0 :
                        color = Fill.RED
                    elif term[3] > 0 :
                        color = Fill.PINK
                    elif (term[0] - term[2]) == 0 :
                        color = Fill.RED
                    elif (term[0] - term[2]) < 0.8 :
                        color = Fill.YELLOW
                    elif term[0] < 0.8 or (term[0] - term[1]) < 0.5 :
                        color = Fill.YELLOW

                    output.stringCell(termString, color)
                output.stringCell(member.jobtitle)
                output.nextRow()
                
                if nodetail : continue                
                for detail in detailAssigns :
                    output.stringCell("")
                    output.stringCell(detail.grade,None,Align.RIGHT)
                    for term in detail.terms:
                        termString = " "
                        if term :
                            termString = '{:.1g}'.format(float(term))
                        output.stringCell(termString, Fill.GRAY)
                    output.stringCell(detail.cont_name)
                    output.setRowDimensions(1)
                    output.nextRow()
            else:
                output.stringCell("")
                output.fillRow(Fill.RED, ['C','N'])

        output.save()
    
if __name__ == '__main__':
    config = JBBot.setup('JBBot.ini')
    
    if JBBot.getOption('-h',True) :
        print(__file__+" usage:")
        print("\t-b: BaseDir")
        print("\t-l: no detail assign")
        print("\t-e: Excel output")
        exit(0)

    if JBBot.getOption('-b', True) == True :
        JBBot.printDir(config.get('data', 'base_dir'))
        exit(0)

    baseDir = JBBot.getOption('-b');
    if not baseDir:
        baseDir = JBDate.getFYString()
        
    checkAssign( config,
                 baseDir,
                 JBBot.getOption('-e',True),
                 JBBot.getOption('-l',True))
