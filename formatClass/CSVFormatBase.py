# -*- coding: utf-8 -*-
import configparser
from collections import namedtuple
import datetime
import os,csv
import re

from openpyxl.utils.cell import *

class CSVFormatBaseError(Exception):
    pass

class CSVFormatBase:

    @staticmethod
    def readFormat(file, name ):
        parser = configparser.ConfigParser(inline_comment_prefixes=('#',';'))
        config_file = re.sub('\.py$', '.ini', file)
        if not os.path.exists(config_file):
            raise CSVFormatBaseError("can't find format config file for %s " % name)
            
        parser.read(config_file)

        Format = namedtuple('Format' , ['index','colname','title','option'])
        format = {}
        try :
            for key in parser['format']:
                each_col = parser.get('format', key).split()
                format[key] = Format( column_index_from_string(each_col[0]) - 1,
                                      each_col[0],
                                      each_col[1] if len(each_col) > 1 else "",
                                      each_col[2] if len(each_col) > 2 else None )
        except KeyError as e:
            raise CSVFormatBaseError("can't find format in %s" % config_file)

        return (format, int(parser.get('file', 'header_rows')))

    def convert(data, format) :
        if format.option :
            if format.option == 'id':
                data = re.sub("^'","",data)
            elif format.option == 'month':
                if not data : data = "'9999/12";
                data = datetime.datetime.strptime(data, "'%Y/%m")
            elif format.option == 'float':
                data = float(data) if data else 0
        return data
