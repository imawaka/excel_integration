# -*- coding: utf-8 -*-
from collections import namedtuple
import openpyxl
from openpyxl.worksheet.dimensions import RowDimension, ColumnDimension
from openpyxl.styles import PatternFill, numbers
from openpyxl.styles.alignment import Alignment
from openpyxl.utils import get_column_letter, column_index_from_string
from openpyxl.styles.borders import Border, Side
from utility import JBBot, JBDate, console
from enum import Enum, auto

ColStyle = namedtuple('ColStyle', ('col', 'width','label','align'))

class Fill():
    YELLOW = 'yellow'
    PINK = 'pink'
    RED = 'red'
    GRAY = 'gray'
    LIGHTYELLOW = 'lightyellow'
    LIGHTGRAY = 'lightgray'

class Align():
    RIGHT  = 'right'
    LEFT   = 'left'
    CENTER = 'center'

class JBOutput():

    bookname = None
    sheetname = None

    workbookPath = None
    excel = False
    
    result_wb = None
    result_ws = None
    rrow = 1
    rcol = 1

    cellstyles = {}

    fillpattern = { Fill.RED: PatternFill(patternType='solid', fgColor='ff9191', bgColor='ff9191'),
                    Fill.PINK: PatternFill(patternType='solid', fgColor='d3d3d3', bgColor='ff1493'),
                    Fill.GRAY: PatternFill(patternType='solid', fgColor='eeeeee', bgColor='eeeeee'),
                    Fill.LIGHTGRAY: PatternFill(patternType='solid', fgColor='dddddd', bgColor='dddddd'),
                    Fill.LIGHTYELLOW: PatternFill(patternType='solid', fgColor='fffbe6', bgColor='fffbe6'), 
                    Fill.YELLOW: PatternFill(patternType='solid', fgColor='ffff91', bgColor='ffff91') }
    
    titlefill = PatternFill(patternType='solid', fgColor='696969', bgColor='696969')
    
    def __init__( self, bookname, sheetname, config) :
        self.bookname = bookname
        self.sheetname = sheetname
        self.cellstyles = {}
        output_dir = config.get('result', 'output_dir')
        typedir = '/'
        result_type = config.get('result', JBBot.appname())
        if result_type :
            typedir = '/' + result_type + '/'
        self.workbookPath = output_dir + typedir + bookname+'.xlsx'
        
    def excelOutput( self, flag ) :
        self.excel = flag
        if self.excel :
            try :
                self.result_wb = openpyxl.load_workbook(self.workbookPath)
                if self.sheetname in self.result_wb.sheetnames :
                    self.result_wb.remove( self.result_wb[self.sheetname] )
                self.result_ws = self.result_wb.create_sheet(title=self.sheetname)
            except FileNotFoundError:
                self.result_wb = openpyxl.Workbook()
                self.result_ws = self.result_wb.active
                self.result_ws.title = self.sheetname

    def headerTitles(self, cellvalues) :
        if self.excel :
            for cellvalue in cellvalues :
                cell = self.result_ws.cell( row=self.rrow, column=column_index_from_string(cellvalue[0]), value=cellvalue[1])
                if len(cellvalue) > 2 :
                    self.result_ws[cell.coordinate].fill = self.fillpattern[cellvalue[2]];
                    side = Side(style='thin', color='ffffff')
                    border = Border(bottom=side, right=side)
                    self.result_ws[cell.coordinate].border = border
                if len(cellvalue) > 3 :
                    cell.alignment = Alignment(horizontal = cellvalue[3], vertical = 'center')
                if len(cellvalue) > 4 :
                    self.result_ws.merge_cells(cellvalue[4][0]+str(self.rrow)+':'+cellvalue[4][1]+str(self.rrow))

            self.nextRow()

    def addHeaderCell(self, label, width, align=None) :
        colname = self.colname(len(self.cellstyles)+1)
        cs = ColStyle(colname, width, label, align if align else Align.LEFT)
        self.cellstyles[cs.col] = cs;
        return colname
    
    def printHeader(self) :
        for cs in self.cellstyles.values() :
            if self.excel :
                self.result_ws.column_dimensions[cs.col].width = cs.width
                self.stringCell(cs.label)
        self.nextRow()
    
    def headerRow(self, cellstyles) :
        for cellstyle in cellstyles :
            cs = ColStyle(cellstyle[0], cellstyle[1], cellstyle[2],
                          cellstyle[3] if len(cellstyle) > 3 else Align.LEFT)
            self.cellstyles[cs.col] = cs;
            if self.excel :
                self.result_ws.column_dimensions[cs.col].width = cs.width
            self.stringCell(cs.label)

        self.nextRow()

    def headerRowFix(self):
        if self.excel :
            self.result_ws.freeze_panes = 'A2'

    def blankCell(self, length ) :
        if not self.excel :
            print(" "*length, end="")
        self.rcol += 1

    def skipCell(self, num, fill:Fill = None) :
        for i in range(num):
            self.stringCell('', fill)
        
    def stringCell(self, value, fill:Fill = None, palign:Align = None, marge = None) :
        cs = self.cellstyles[self.colname(self.rcol)]
        width = cs.width if cs else 8
        align = cs.align if cs else Align.LEFT
        if palign : align = palign
        if self.excel :
            if marge :
                self.result_ws.merge_cells(marge[0]+str(self.rrow)
                                           +':'+marge[1]+str(self.rrow))
            cell = self.result_ws.cell( row=self.rrow, column=self.rcol, value=value )
            if marge :
                self.rcol = openpyxl.utils.column_index_from_string(marge[1])
            cell.alignment = Alignment(horizontal = align, vertical = 'center')
            if fill :
                self.result_ws[cell.coordinate].fill = self.fillpattern[fill];
                side = Side(style='thin', color='ffffff')
                border = Border(bottom=side, right=side)
                self.result_ws[cell.coordinate].border = border
        else :
            self.printConsole(value, width, align)
        self.rcol += 1

    def moneyCalcCell(self, mvalue, dvalue, fill:Fill = None, palign:Align = None) :
        value = mvalue/dvalue if dvalue > 0 else 0
        self.moneyCell(value, fill, palign)
        
    def moneyCell(self, value, fill:Fill = None, palign:Align = None, marge = None) :
        cs = self.cellstyles[self.colname(self.rcol)]
        width = cs.width if cs else 8
        align = cs.align if cs else Align.RIGHT
        if palign : align = palign
        if self.excel :
            if marge :
                self.result_ws.merge_cells(marge[0]+str(self.rrow)
                                           +':'+marge[1]+str(self.rrow))
            cell = self.result_ws.cell( row=self.rrow, column=self.rcol, value=value )
            if marge :
                self.rcol = openpyxl.utils.column_index_from_string(marge[1])
            cell.number_format = '#,##0'
            cell.alignment = Alignment(horizontal = align, vertical = 'center')
            if fill :
                self.result_ws[cell.coordinate].fill = self.fillpattern[fill];
            if value and value < 0 :
                cell.font = openpyxl.styles.fonts.Font(color='ff0000')
        else :
            self.printConsole(console.money(value), width, align)
        self.rcol += 1

    def ratioCalcCell(self, mvalue, dvalue, fill:Fill = None, palign:Align = None) :
        value = mvalue/dvalue if dvalue > 0 else 0
        self.ratioCell(value, fill, palign)
        
    def ratioCell(self, value, fill:Fill = None, palign:Align = None) :
        cs = self.cellstyles[self.colname(self.rcol)]
        width = cs.width if cs else 8
        align = cs.align if cs else Align.RIGHT
        if palign : align = palign
        if self.excel :
            cell = self.result_ws.cell( row=self.rrow, column=self.rcol, value=value )
            cell.number_format = numbers.FORMAT_PERCENTAGE
            cell.alignment = Alignment(horizontal = align, vertical = 'center')
            if fill :
                self.result_ws[cell.coordinate].fill = self.fillpattern[fill];
        else :
            self.printConsole(console.ratio(value), width, align)
        self.rcol += 1

    def fillRow(self, fill, marge = None ):
        if self.excel:
            if marge :
                self.result_ws.merge_cells(marge[0]+str(self.rrow)+':'+marge[1]+str(self.rrow))
            cell = self.result_ws.cell( row=self.rrow, column=self.rcol )
            self.result_ws[cell.coordinate].fill = self.fillpattern[fill]
        else :
            print("")
            
        self.rrow += 1
        self.rcol = 1

    def titleCell(self, value, marge = None) :
        if self.excel :
            if marge :
                self.result_ws.merge_cells(marge[0]+str(self.rrow)+':'+marge[1]+str(self.rrow))
            cell = self.result_ws.cell( row=self.rrow, column=1, value=value )
            if marge :
                self.rcol = openpyxl.utils.column_index_from_string(marge[1])
            cell.font = openpyxl.styles.fonts.Font(color='ffffff')
            self.result_ws[cell.coordinate].fill = self.titlefill
        else :
            print(value,end="")
        self.rcol += 1
            
    def titleRow(self, value, marge = None) :
        if self.excel :
            if marge :
                self.result_ws.merge_cells(marge[0]+str(self.rrow)+':'+marge[1]+str(self.rrow))
            cell = self.result_ws.cell( row=self.rrow, column=1, value=value )
            cell.font = openpyxl.styles.fonts.Font(color='ffffff')
            self.result_ws[cell.coordinate].fill = self.titlefill
        else :
            print(value)
        self.rrow += 1
        self.rcol = 1

    def printConsole( self, value, width, align: Align ):
        if align == Align.RIGHT :
            print( console.right(value, width), end="" )
        elif align == Align.LEFT :
            print( console.left(value, width), end="" )
        elif align == Align.CENTER :
            print( console.center(value, width), end="" )
        else :
            print(value, end="" )

    def setRowDimensions(self,level):
        if self.excel :
            self.result_ws.row_dimensions[self.rrow] = RowDimension(self.result_ws,
                                                                    index=self.rrow,
                                                                    outline_level=level,
                                                                    hidden=(level != 0))
    def hideCol(self, col) :
        if self.excel :
            self.result_ws.column_dimensions[col] = ColumnDimension(self.result_ws,
                                                                   index=col,
                                                                   outline_level=1,
                                                                   hidden=True)
    def freeze(self, cellname) :
        if self.excel :
            self.result_ws.freeze_panes = cellname
            
    def nextRow(self) :
        if not self.excel :
            print("")
        self.rcol = 1
        self.rrow += 1

    def save(self):
        if self.excel:
            print('save excel')
            self.result_wb.save(self.workbookPath)
        
    def colname( self, index ) :
        return get_column_letter(index)
